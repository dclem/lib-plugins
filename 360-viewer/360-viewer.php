<?php

/*
Plugin Name: 360 Product Viewer for MG
Description: Added a custom 360 product viewer
Version: 1.0.0
Text Domain: mg-360-viewer
Author: Damian Clem
*/


if ( ! defined( 'MG_360_VIEWER_DIR' ) ) {
	define( 'MG_360_VIEWER_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
}

if ( ! defined( 'MG_360_VIEWER_URI' ) ) {
	define( 'MG_360_VIEWER_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );
}

/**
 * Enqueue scripts and styles.
 */
function mg_360_viewer_scripts() {

    wp_enqueue_script( 'mg-360-viewer-createjs-script', MG_360_VIEWER_URI . 'js/createjs.js', array( 'jquery' ), true );
    wp_enqueue_script( 'mg-360-viewer-scripts', MG_360_VIEWER_URI . 'js/360-viewer.js', array( 'jquery' ), true );

    wp_enqueue_style( 'mg-360-viewer-grid', MG_360_VIEWER_URI . 'css/simple-grid.min.css' );
    wp_enqueue_style( 'mg-360-viewer-styles', MG_360_VIEWER_URI . 'css/360-viewer.css' );

}

add_action( 'wp_enqueue_scripts', 'mg_360_viewer_scripts' );


// [bartag foo="foo-value"]
function mg_360_viewer_func( $atts ) {

    ob_start();

    include plugin_dir_path(__FILE__) . 'partials/viewer.php';

    // $a = shortcode_atts( array(
    //     'foo' => 'something',
    //     'bar' => 'something else',
    // ), $atts );

    return ob_get_clean();
}

add_shortcode( 'mg-360-viewer', 'mg_360_viewer_func' );