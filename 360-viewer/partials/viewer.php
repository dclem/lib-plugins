<?php

?>

<div id="viewer-path" data-path="<?=MG_360_VIEWER_URI?>"></div>

<div class="row product-viewer-container">
    <div class="col-3 product-selection-section">
        <h3>Select Strap Style</h3>
        <ul>
            <li><a class="style-selection" data-style="classic" href="#">Classic</a></li>
            <li><a class="style-selection" data-style="classic-xl" href="#">XL Classic</a></li>
            <li><a class="style-selection" data-style="designer" href="#">Designer</a></li>
            <li><a class="style-selection" data-style="handmade" href="#">Handmade</a></li>
        </ul>
        
        <h3>Select Strap Color</h3>
        <ul id="product-color-selector" class="product-color-selector"></ul>

    </div>
    <div class="col-5">
        <canvas id='canvas' width="500" height="500"></canvas>
    </div>
    <div class="col-4 product-information-section">
        <img src="<?=MG_360_VIEWER_URI?>images/stick-it-magnetic-strap.png" alt="">
        <h3 id="product-name">Dress up your Range Finder</h3>
        <p id="product-description">Select a Stick It Strap Style on the left.  Then select a color.  Once you have the style and color or your choosing, click the puchase button.</p>
    </div>
</div>

</html>