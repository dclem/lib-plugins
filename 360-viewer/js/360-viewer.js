jQuery(function () {

    var pluginPath = jQuery("#viewer-path").attr("data-path");

    strapData = {
        classic: {
            fullName: 'Classic Series Specs',
            desc: '<p>Rugged nylon material weathers all conditions and resists rips and tears.</p><p><strong>Adjustable Strap</strong> – Fits snug on all standard-sized rangefinder models including Bushnell, Callaway, Leupold, Nikon and more.</p> <p><strong>Super-Strong Hold</strong> – Industrial-strength neodymium magnets provide a secure hold and withstand sharp turns, bumps and sudden stops.</p>',
            colors: [{
                black: {
                    name: 'Black Classic Strap',
                    url: 'http://www.monumentproducts.com/product/classic-stick-it-strap-black/'
                },
                blue: {

                },
                green: {

                },
                red: {

                }
            }]
        },
        xlclassic: {
            fullName: 'XL Classic Series Specs',
            desc: '<p>Specifically designed for large or over-sized rangefinder models, notably the Bushnell Pinseeker 1500, PRO 1600, Elite 1500, PRO X7 and Medalist models.</p><p><strong>Stronger & Longer</strong> – Boasts bigger magnets for a stronger hold, a longer and wider design to accommodate larger model sizes, and a reinforced magnet patch for increased stability.</p><p> <strong>High-Quality Construction</strong> – Adjustable strap constructed of rugged nylon.</p><p> <strong>Super-Strong Hold</strong> – Industrial-strength neodymium magnets provide a secure hold and withstand sharp turns, bumps and sudden stops.</p>',
            colors: [
                'black',
                'red',
            ]
        },
        designer: {
            fullName: 'Designer Series Specs',
            desc: '<p>Rugged nylon is finished with treated leather for a designer look.</p><p><strong>Leather Strap Material</strong> – Treated leather weathers all course conditions.</p><p><strong>Adjustable Strap</strong> – Fits snug on all standard-sized rangefinder models including Bushnell, Callaway, Leupold, Nikon and more.</p><p><strong>Super-Strong Hold</strong> – Industrial-strength neodymium magnets provide a secure hold and withstand sharp turns, bumps and sudden stops.</p>',
            colors: [
                'aussie',
                'bayou',
                'camo',
                'carbon',
                'pink'
            ]
        },
        handmade: {
            fullName: 'Handmade Series Specs',
            desc: '<p>Unique and stylish handmade straps from a true craftsman, Patrick Gibbons Handmade. Using the best materials nature has to offer, these premium straps are made from exotic materials and are hand sewn and assembled.</p><p><strong>Premium Handmade Material</strong> – Exotics are hand-selected, hand-trimmed and hand-prepared.</p><p><strong>Limited Edition</strong> – Exotic pattern and texture is unique to each individual strap, meaning every handmade strap is one-of-a-kind.</p><p><strong>Adjustable Strap</strong> – Fits snug on all standard-sized rangefinder models.</p><p><strong>Super-Strong Hold</strong> – Industrial-strength neodymium magnets provide a secure hold and withstand sharp turns, bumps and sudden stops.</p>',
            colors: [
                'aligator-color1',
                'aligator-color2',
                'aligator-color3',
                'aligator-color4',
                'bison-color1',
            ]
        },

    }

    var images = [],
        imageCollection = [],
        loaded = 0,
        currentFrame = 0,
        totalFrames = 0;
    var rotate360Interval, start_x;
    var bg, bmp, logoBmp, logoImage;

    var stage;

    var canvas = document.getElementById("canvas");

    var view360 = {

        init: function () {

            if (!canvas || !canvas.getContext) return;

            var ctx = canvas.getContext('2d');

            stage = new createjs.Stage(canvas);
            stage.enableMouseOver(true);
            stage.mouseMoveOutside = true;
            createjs.Touch.enable(stage);

            progress = new createjs.Shape();
            stage.addChild(progress);

            logoImage = new Image();
            logoImage.src = pluginPath + "images/mg-logo.png";
            logoImage.onload = view360.logoLoaded;

            // 360 icon
            var iconImage = new Image();
            iconImage.src = pluginPath + "assets/360-logo.png";
            iconImage.onload = view360.iconLoaded;

            bmp = new createjs.Bitmap();
            stage.addChild(bmp);

            // TICKER
            createjs.Ticker.addEventListener("tick", view360.handleTick);
            createjs.Ticker.setFPS(60);
            createjs.Ticker.useRAF = true;

            // Set inital 360 image and set the global images array
            img = view360.getSelectedImage('designer', 'camo');
            view360.load360Image(img);


        },

        load360Image: function (image) {

            // totalFrames = imgs.length

            // var img = new Image();
            // img.src = imgs[loaded];
            // img.onload = view360.img360Loaded;
            // images[loaded] = img;

            logoImage = new Image();
            logoImage.src = image
            logoImage.onload = view360.logoLoaded;


        },

        img360Loaded: function (event) {
            loaded++;

            progress.graphics.clear()
            progress.graphics.beginFill("#fce333").drawRect(0, 0, stage.canvas.width * loaded / totalFrames, 5);
            progress.graphics.endFill();

            if (loaded == totalFrames) {
                view360.start360();
                logoBmp.visible = false;
            } else {
                view360.load360Image();
                logoBmp.visible = true;
            }

        },

        start360: function () {

            document.body.style.cursor = 'none';

            progress.graphics.clear()

            // update-draw
            view360.update360(0);

            // // first rotation
            // rotate360Interval = setInterval(function () {
            //     if (currentFrame === totalFrames - 1) {
            //         clearInterval(rotate360Interval);
            //         view360.addNavigation();
            //     }
            //     view360.update360(1);
            // }, 25);
        },

        logoLoaded: function () {
            logoBmp = new createjs.Bitmap();
            logoBmp.image = event.target;
            logoBmp.x = 0;
            logoBmp.y = 10;
            stage.addChild(logoBmp);
        },

        iconLoaded: function (event) {
            var iconBmp = new createjs.Bitmap();
            iconBmp.image = event.target;
            iconBmp.x = 0;
            iconBmp.y = 10;
            stage.addChild(iconBmp);
        },

        // Product image (Static)
        productImageLoaded: function () {
            var productImg = new createjs.Bitmap();
            productImg.image = event.target;
            productImg.x = 0;
            productImg.y = 10;
            stage.addChild(productImg);
        },

        update360: function (dir) {
            currentFrame += dir;
            if (currentFrame < 0) currentFrame = totalFrames - 1;
            else if (currentFrame > totalFrames - 1) currentFrame = 0;
            bmp.image = images[currentFrame];
            bmp.x = 0;
            bmp.y = 10;
        },

        addNavigation: function () {
            stage.onMouseOver = view360.mouseOver;
            stage.onMouseDown = view360.mousePressed;
            document.body.style.cursor = 'auto';
        },

        mouseOver: function (event) {
            document.body.style.cursor = 'ew-resize';
        },

        mousePressed: function (event) {
            start_x = event.rawX;
            stage.onMouseMove = view360.mouseMoved;
            stage.onMouseUp = view360.mouseUp;
            document.body.style.cursor = 'ew-resize';
        },

        mouseMoved: function (event) {
            var dx = event.rawX - start_x;
            var abs_dx = Math.abs(dx);

            if (abs_dx > 5) {
                view360.update360(abs_dx / dx);
                start_x = event.rawX;
            }
        },

        mouseUp: function (event) {
            stage.onMouseMove = null;
            stage.onMouseUp = null;
            // document.body.style.cursor = 'pointer';
        },

        handleTick: function () {
            stage.update();
        },

        getSelectedImages: function (style, color) {

            var imgCollection = [];

            for (i = 0; i < 59; i++) {

                incrementvalue = ("00000" + i).slice(-5);

                imgCollection.push(pluginPath + "assets/" + style + "/" + color + "/" + incrementvalue + ".jpg");
            }

            return imgCollection;

        },

        getSelectedImage: function(style, color) {

           return pluginPath + "assets/" + style + "/" + color + "/" + "0.jpg"

        }

    }

    // Init the 360 Product Viewer
    window.addEventListener('load', view360.init, false);


    // Handle onclick events for style, color and link to product.
    jQuery(".style-selection").on('click', function (e) {

        var style = jQuery(this).attr('data-style');
        var availableColors = [];
        var colors = [];
        loaded = 0;

        jQuery("#product-color-selector").empty();

        switch (style) {
            case 'classic':
                var name = strapData.classic.fullName;
                var desc = strapData.classic.desc;
                var colors = strapData.classic.colors;

                // imgs = view360.getSelectedImages(style, 'black');
                img = view360.getSelectedImage(style, 'black');
                view360.load360Image(img);
                break;
            case 'classic-xl':
                var name = strapData.xlclassic.fullName;
                var desc = strapData.xlclassic.desc;
                var colors = strapData.xlclassic.colors;

                // imgs = view360.getSelectedImages(style, 'black');
                img = view360.getSelectedImage(style, 'black');
                view360.load360Image(img);
                break;
            case 'designer':
                var name = strapData.designer.fullName;
                var desc = strapData.designer.desc;
                var colors = strapData.designer.colors;

                // imgs = view360.getSelectedImages(style, 'black');
                img = view360.getSelectedImage(style, 'aussie');                                
                view360.load360Image(img);
                break;
            case 'handmade':
                var name = strapData.handmade.fullName;
                var desc = strapData.handmade.desc;
                var colors = strapData.handmade.colors;

                // imgs = view360.getSelectedImages(style, 'aligator-color1');
                img = view360.getSelectedImage(style, 'aligator-color1');                
                view360.load360Image(img);
                break;

            default:
                console.log('style does not exist');
                break;
        }


        jQuery.each(colors, function (i, color) {

            console.log(color);

            availableColors.push('<li><a  data-style="' + style + '" data-color="' + color + '" class="' + style + '-' + color + ' selected-color" href="#"></a></li>');

        });

        jQuery("#product-name").html(name);
        jQuery("#product-description").html(desc);
        jQuery("#product-color-selector").append(availableColors.join(''));

        e.preventDefault();

    });


    jQuery(document).on('click', 'a.selected-color', function (e) {

        var selectedStyle = jQuery(this).attr('data-style');
        var selectedColor = jQuery(this).attr('data-color');
        imgs = [];
        loaded = 0;

        img = view360.getSelectedImage(selectedStyle, selectedColor);
        // imgs = view360.getSelectedImages(selectedStyle, selectedColor);

        view360.load360Image(img);

        e.preventDefault();

    })

});

// Init