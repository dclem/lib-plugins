<?php


class Lib_Gallery_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'lib_gallery',
			'description' => 'A Gallery Widget',
		);
		add_action('wp_ajax_lib_gallery_getImages', array(&$this, 'getLibGalleryImages'));

		parent::__construct( 'lib_gallery', 'LiB Gallery', $widget_ops );
	}

	public function cmp($a, $b){
		return strcmp($a->order, $b->order);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		//TODO: handle logic for different social channel types.
		// Twitter -> create twitter link
		// Instagram -> create instagram link
		// url -> create url (maybe clean up the URL? if facebook url make link say "Facebook" same for "Youtube")

		$direction = !empty($instance['direction']) ? $instance['direction'] : 'ltr';
		$images = ! empty( $instance['images'] ) ? $instance['images'] : '';
		$imagesObj = json_decode($images);
		// $imgArray = new ArrayObject($imagesObj);
		$orderedImgObjs = (array) $imagesObj;

		wp_enqueue_style( 'lib-gallery_frontend' );
		usort($orderedImgObjs, array($this, "cmp"));
		$uniqueID = uniqid();

		if(count($orderedImgObjs) > 0){
			?><div class="gallery-module <?php echo $direction; ?>" dir="<?php echo $direction; ?>"><?php

			foreach($orderedImgObjs as $key => $value){
				// echo '<pre>';
				// print_r($value);
				// echo '</pre>';
				$artistURL = '';
				$artistLinkType = '';
				switch($value->socialChannelSelection){
					case 'twitter':
						$artistURL = 'https://twitter.com/' . $value->twitterHandle;
						$linkText = '@'.$value->twitterHandle;
						break;
					case 'instagram':
						$artistURL = 'https://www.instagram.com/' . $value->instagramUsername;
						$linkText = '@'.$value->instagramUsername;
						break;
					case 'url':
						$artistURL = $value->artistUrl;
						if (strpos($value->artistUrl, 'facebook') !== false) {
							$linkText = 'Facebook';
						}else if(strpos($value->artistUrl, 'youtube') !== false) {
							$linkText = 'Youtube';
						}
						$linkText = $value->artistUrl;
						break;
					default:
						$artistURL = '';
						$linkText = '';
				}

				$imageFull  = wp_get_attachment_image_src($value->id, 'full');
				// print_r($image);
				?>
				<figure href="<?php echo $imageFull[0]; ?>" data-fancybox="gallery_<?php echo $uniqueID; ?>" data-caption="<h2><?php echo $value->title; ?></h2><p><a href='<?php echo $artistURL; ?>' target='_blank'><?php echo $linkText; ?></a></p>" data-src="<?php echo $imageFull[0]; ?>">
					<figcaption>
						<h2><?php echo $value->title; ?></h2>
						<p><a href="<?php echo $artistURL; ?>" target="_blank"><?php echo $linkText; ?></a></p>
					</figcaption>
				</figure>
				<?php
			}
		}
		?></div><?php
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$direction = ! empty( $instance['direction'] ) ? $instance['direction'] : 'ltr';
		$images = ! empty( $instance['images'] ) ? $instance['images'] : '{}';
		$imagesObj = json_decode($images);
		$orderedImgObjs = (array) $imagesObj;
		$uniqueID = uniqid();
		usort($orderedImgObjs, array($this, "cmp"));

		?>
		<div id="lib_gallery_form_<?php echo $uniqueID; ?>" class="lib_gallery_form_container grid-container">
			<div class="grid-x">
				<fieldset class="cell small-3">
					<legend>Switch Gallery Direction</legend>
					<input type="radio" id="directionChoice1" name="<?php echo $this->get_field_name('direction'); ?>" value="ltr" <?php checked($direction, 'ltr', true); ?>>
					<label for="directionChoice1">Left To Right</label>

					<input type="radio" id="directionChoice2" name="<?php echo $this->get_field_name('direction'); ?>" value="rtl" <?php checked($direction, 'rtl', true); ?>>
					<label for="directionChoice2">Right To Left</label>
				</fieldset>
				<div class="cell small-9">
					<a class="button-secondary" id="lib_gallery_addImages" href="#" ><?php esc_attr_e( 'Add Images' ); ?></a>
				</div>
			</div>

			<div id="lib_gallery_images" class="grid-x grid-margin-x">
				<?php
				if(count($orderedImgObjs) > 0){
					foreach($orderedImgObjs as $key => $value){
						$image  = wp_get_attachment_image_src($value->id, 'full');

						?>
						<div class="cell small-2 imgBlockContainer" id="img_block_<?php echo $value->id; ?>">
							<a class="lib-admin_ui--image_item--remove" data-parentid="lib_gallery_form_<?php echo $uniqueID; ?>" data-id="<?php echo $value->id; ?>" href="#" title="Remove">X</a>
							<div class="img-preview" style="background-image: url(<?php echo $image[0]; ?>);">
							</div>

							<input type="text" id="title" name="<?php echo $value->id; ?>" placeholder="Artist Name" value="<?php echo $value->title; ?>" />
							<label>Social Channel
								<select id="socialChannelSelection" name="<?php echo $value->id; ?>">
									<option>----</option>
									<option value="twitter" <?php selected( $value->socialChannelSelection, 'twitter' ); ?>>Twitter</option>
									<option value="instagram" <?php selected( $value->socialChannelSelection, 'instagram' ); ?>>Instagram</option>
									<option value="url" <?php selected( $value->socialChannelSelection, 'url' ); ?>>URL</option>
								</select>
							</label>
							<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'twitter') echo 'active'; ?>" id="twitter">
								<span class="input-group-label">@</span>
								<input name="<?php echo $value->id; ?>" id="twitterHandle" class="input-group-field" placeholder="Twitter Handle" value="<?php echo $value->twitterHandle; ?>"/>
							</div>
							<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'instagram') echo 'active'; ?>" id="instagram">
								<span class="input-group-label">@</span>
								<input name="<?php echo $value->id; ?>" id="instagramUsername" class="input-group-field" placeholder="Instagram Username" value="<?php echo $value->instagramUsername; ?>"/>
							</div>
							<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'url') echo 'active'; ?>" id="url">
								<input name="<?php echo $value->id; ?>" id="artistUrl" class="input-group-field" placeholder="https://example.com" value="<?php echo $value->artistUrl; ?>"/>
							</div>


							<label for="order">Order
								<input type="number" id="order" min="1" name="<?php echo $value->id; ?>" value="<?php echo $value->order; ?>" />
							</label>
							<input type="hidden" id="" name="" value="<?php echo $value->id; ?>" />

						</div>
						<?php
					}
				}else{
					?>
					<div class="cell noImgs">
						<h3>No images selected</h3>
					</div>
					<?php
				}
				?>
			</div>
			<input type="hidden" id="lib_gallery_imgsObj" name="<?php echo $this->get_field_name('images'); ?>" value='<?php echo $images; ?>' />
		</div>

		<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['direction'] = ( ! empty( $new_instance['direction'] ) ) ? strip_tags( $new_instance['direction'] ) : '';

		$instance['images'] = ( ! empty( $new_instance['images'] ) ) ? strip_tags( $new_instance['images'] ) : '';

		return $instance;
	}

	public function getLibGalleryImages(){
		$images = json_decode($_POST['imageids']);
		$parentID = $_POST['parentId'];
		if(count($images) > 0){
			foreach($images as $key => $value){
				$image  = wp_get_attachment_image_src($value, 'full');

				?>
				<div class="cell small-2 imgBlockContainer" id="img_block_<?php echo $value; ?>">
					<a class="lib-admin_ui--image_item--remove" data-parentid="<?php echo $parentID; ?>" data-id="<?php echo $value; ?>" href="#" title="Remove">X</a>
					<div class="img-preview" style="background-image: url(<?php echo $image[0]; ?>);">
					</div>

					<input type="text" id="title" name="<?php echo $value; ?>" placeholder="Artist Name" value="<?php echo $value->title; ?>" />
					<label>Social Channel
						<select id="socialChannelSelection" name="<?php echo $value; ?>">
							<option>----</option>
							<option value="twitter" <?php selected( $value->socialChannelSelection, 'twitter' ); ?>>Twitter</option>
							<option value="instagram" <?php selected( $value->socialChannelSelection, 'instagram' ); ?>>Instagram</option>
							<option value="url" <?php selected( $value->socialChannelSelection, 'url' ); ?>>URL</option>
						</select>
					</label>
					<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'twitter') echo 'active'; ?>" id="twitter">
						<span class="input-group-label">@</span>
						<input name="<?php echo $value; ?>" id="twitterHandle" class="input-group-field" placeholder="Twitter Handle" value="<?php echo $value->twitterHandle; ?>"/>
					</div>
					<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'instagram') echo 'active'; ?>" id="instagram">
						<span class="input-group-label">@</span>
						<input name="<?php echo $value; ?>" id="instagramUsername" class="input-group-field" placeholder="Instagram Username" value="<?php echo $value->instagramUsername; ?>"/>
					</div>
					<div class="input-group socialLink <?php if($value->socialChannelSelection != '' && $value->socialChannelSelection == 'url') echo 'active'; ?>" id="url">
						<input name="<?php echo $value; ?>" id="artistUrl" class="input-group-field" placeholder="https://example.com" value="<?php echo $value->artistUrl; ?>"/>
					</div>


					<label for="order">Order
						<input type="number" id="order" min="1" name="<?php echo $value; ?>" value="1" />
					</label>
					<input type="hidden" id="" name="" value="<?php echo $value; ?>" />

				</div>
				<?php
			}
		}

		wp_die();
	}
}