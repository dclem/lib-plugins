<?php
/**
 * LiB Gallery
 *
 * @package   LibGallery
 * @author    Tomas Cordero
 * @copyright Copyright (c) 2017 Tomas, LLC
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: LiB Gallery
 * Plugin URI:
 * Description: A Gallery plugin for Life Is Beautiful
 * Version:     1.0.0
 * Author:      Tomas Cordero
 * Author URI:  https://rrpartners.com
 * License:     GPL-2.0+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: lib-gallery
 */

wp_register_style(
	'lib-gallery',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-gallery/assets/css/lib-gallery.css'
);
wp_register_style(
	'simple-grid',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-gallery/assets/css/simple-grid.min.css'
);
wp_register_style(
	'lib-gallery_frontend',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-gallery/assets/css/styles.css'
);

wp_register_script(
	'lib-gallery',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-gallery/assets/js/lib-gallery.js',
	array( 'media-upload', 'media-views', 'wp-backbone', 'wp-util', 'jquery' )
);


require_once(plugin_dir_path(__FILE__).'/includes/lib-gallery-widget.php');

function enqueue_media_uploader()
{
    wp_enqueue_script( 'lib-gallery' );
	wp_enqueue_style( 'lib-gallery' );
	// wp_enqueue_style( 'simple-grid' );
	wp_enqueue_style( 'dashicons' );
}
function register_libGallery(){
	register_widget('Lib_Gallery_Widget');
}
add_action("admin_enqueue_scripts", "enqueue_media_uploader");
add_action('widgets_init', 'register_libGallery');