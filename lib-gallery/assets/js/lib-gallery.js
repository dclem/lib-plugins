var media_uploader = null;
var admin_thumb_ul = '';
function libMediaUploader(selector)
{
    admin_thumb_ul = selector;
    media_uploader = wp.media({
        frame:    "post",
        state:    "insert",
        library: {type: 'image'},
        multiple: true
    });
    media_uploader.on("insert", function(){

        // var length = media_uploader.state().get("selection").length;
        var images = media_uploader.state().get('selection').toJSON();
        // console.log(images);
		// var selectedImages = [];
        var $hiddenInput = selector.find('#lib_gallery_imgsObj'),
            existingImgs = ($hiddenInput.val() != '{}' || $hiddenInput.val() != '') ? JSON.parse($hiddenInput.val()) : {};
        var imageIds = [];
        jQuery.each(images, function(key, data){
            // console.log(data.id);
            if(typeof existingImgs[data.id] === 'undefined'){
                imageIds.push(data.id);
            }
        })
        libGalleryUpdateHiddenField(imageIds, selector);
        libGalleryPassImages(JSON.stringify(imageIds), selector);
		// generate_media_widget_ui(selectedImages);
    });
    media_uploader.open();
};
function libGalleryUpdateHiddenField(ids, selector){
    var $hiddenInput = selector.find('#lib_gallery_imgsObj'),
        image = ($hiddenInput.val() != '{}' || $hiddenInput.val() != '') ? JSON.parse($hiddenInput.val()) : {};
    var objs = []
    jQuery.each(ids, function(key, value){
        image[value] = {};
        image[value].id = value;
        image[value].title = '';
        image[value].twitterHandle = '';
        image[value].order = '1';
    })
    $hiddenInput.val(JSON.stringify(image));


}
function libGalleryPassImages(ids, selector) {

    var data = {
        action: 'lib_gallery_getImages',
        imageids: ids,
        parentId: selector.attr('id')
    };
    jQuery.post(ajaxurl, data, function(response) {
        admin_thumb_ul.find('.noImgs').remove();
        admin_thumb_ul.find('#lib_gallery_images').append(response);
    });
}



$(function(){
	$('body').on('click', '#lib_gallery_addImages', function(e){
        e.preventDefault();
        libMediaUploader($(this).closest('.lib_gallery_form_container'));
    });
	$('body').on('click', '.lib-admin_ui--image_item--remove', function(e){
        e.preventDefault();
        // console.log('test');
        if(confirm("Are you sure you want to remove this image?")){

            var json = JSON.parse($('body').find('#' + $(this).data('parentid')).find('#lib_gallery_imgsObj').val());
            delete json[$(this).data('id')];
            $('body').find('#' + $(this).data('parentid')).find('#img_block_' + $(this).data('id')).remove();
            $('body').find('#' + $(this).data('parentid')).find('#lib_gallery_imgsObj').val(JSON.stringify(json));
        }
    });
    $('body').on('blur', '.lib_gallery_form_container input, .lib_gallery_form_container textarea', function(){
        var json = JSON.parse($(this).closest('.lib_gallery_form_container').find('#lib_gallery_imgsObj').val());
        json[$(this).attr('name')][$(this).attr('id')] = $(this).val();
        $(this).closest('.lib_gallery_form_container').find('#lib_gallery_imgsObj').val(JSON.stringify(json));
    })

    $('body').on('change', '.lib_gallery_form_container #socialChannelSelection', function(e){
        var $this = $(this);
        var $thisParent = $this.closest('.lib_gallery_form_container');
        var json = JSON.parse($this.closest('.lib_gallery_form_container').find('#lib_gallery_imgsObj').val());
        json[$this.attr('name')][$this.attr('id')] = $this.val();
        console.log(json[$this.attr('name')][$this.attr('id')]);
        $thisParent.find('#lib_gallery_imgsObj').val(JSON.stringify(json));;
        $thisParent.find('#' + $this.closest('.lib_gallery_form_container .imgBlockContainer').attr('id') + ' .socialLink.active').removeClass('active');
        console.log($thisParent.find('#' + $this.closest('.lib_gallery_form_container .imgBlockContainer').attr('id') + ' .socialLink#' + $(this).val()), $(this).val());
        $thisParent.find('#' + $this.closest('.lib_gallery_form_container .imgBlockContainer').attr('id') + ' .socialLink#' + $(this).val()).addClass('active');
    })


})