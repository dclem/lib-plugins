$(function () {
	$('.gallery-module.ltr').slick({
		infinite: true,
		speed: 250,
		centerMode: false,
		variableWidth: true,
		easing: 'easeOutExpo',
		lazyLoad: 'ondemand',
		prevArrow: '',
		nextArrow: '<div class="nextArrow"><span></span></div>',
		responsive: [{
			breakpoint: 600,
			settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			}
		}]
	});
	$('.gallery-module').on('init', function(){
		$('img.lazyload').Lazy({
			beforeLoad: function(element){
				element.closest('.gallery-module').find('figure.slick-cloned img').each(function(){
					$(this).attr('src', $(this).data('src'));
				})
			}
		});
	})

	// TODO: Get this working for mobile
	// $('.gallery-module').on('breakpoint', function(){
	// 	console.log($(this).find('figure'));
	// })

	$('.gallery-module.rtl').slick({
		infinite: true,
		speed: 250,
		centerMode: false,
		variableWidth: true,
		easing: 'easeOutExpo',
		lazyLoad: 'ondemand',
		rtl: true,
		prevArrow: '',
		nextArrow: '<div class="prevArrow"><span></span></div>',
		responsive: [
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
		]
	});
	$('.gallery-module').on('beforeChange', function () {
		$(this).find('img').each(function () {
			if (typeof $(this).attr('src') == 'undefined') {
				$(this).attr('src', $(this).data('src'));
			}
		})

	});
	$('.news-feed').on('beforeChange', function () {
		$(this).find('.news-feed_item').each(function () {
			if (typeof $(this).data('src') == 'string') {
				$(this).css('background-image', 'url('+$(this).data('src')+')');
			}
		})

	})
	$('.news-feed').on('init', function(){
		$('.news-feed_item').Lazy();
	});
	$('.news-feed').on('breakpoint', function(){
		// $('.news-feed_item').Lazy();
		console.log('test')
	});
	$('.news-feed').slick({
		infinite: false,
		speed: 250,
		centerMode: false,
		slidesToShow: 4,
		prevArrow: '<div class="prevArrow"><span></span></div>',
		nextArrow: '<div class="nextArrow"><span></span></div>',
		responsive: [
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 1160,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
		]
	})
})