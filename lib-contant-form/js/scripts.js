(function ($) {


	/** Document Ready */
	$(document).ready(function () {
		
	});

})(jQuery);

function validateFormSubmit() {

	console.log('clicked');

	var fname = jQuery("#firstname");
	var lname = jQuery("#lastname");
	var email = jQuery(".emailinput");
	var form = jQuery("#_form_5_");

	if (fname.val() == "") {
		fname.focus();
		jQuery(".form-error").css("display", "block");
	} else if (lname.val() == "") {
		lname.focus();
		jQuery(".form-error").css("display", "block");
	} else if (email.val() === "") {
		email.focus();
		jQuery(".form-error").css("display", "block");
	} else {
		form.submit();
	}

	return false;

}