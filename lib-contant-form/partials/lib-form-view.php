<?php

?>

<form method="POST" onsubmit="return validateFormSubmit()" action="https://lifeisbeautiful.activehosted.com/proc.php" name="emailsignup"
    id="_form_5_" class="emlform _form _form_5" enctype="application/x-www-form-urlencoded" novalidate="">

    <input type="hidden" name="u" value="5" data-name="u">
    <input type="hidden" name="f" value="5" data-name="f">
    <input type="hidden" name="s" data-name="s">
    <input type="hidden" name="c" value="0" data-name="c">
    <input type="hidden" name="m" value="0" data-name="m">
    <input type="hidden" name="act" value="sub" data-name="act">
    <input type="hidden" name="v" value="2" data-name="v">

    <div class="form-main-container">
        <div class="outer-form-wrapper">
            <div class="_form-content">
                <div class="form-error">Enter all required information</div>

                <div class="title-container">
                    <?=$main_title;?>
                    <?=$sub_title;?>
                </div>

                <div class="field half first">
                    <input name="firstname" id="firstname" type="text" placeholder="First Name*" required="" data-name="firstname">
                </div>

                <div class="field half">
                    <input name="lastname" id="lastname" type="text" placeholder="Last Name*" required="" data-name="lastname">
                </div>

                <div class="field half first">
                    <input class="emailinput" name="email" id="email" type="text" placeholder="Email Address*" required="" data-name="email">
                </div>

                <div class="field half">
                    <input name="phone" id="phone" type="text" placeholder="Mobile Phone" data-name="phone">
                    <span class="policy-copy" style="font-size: 12px; color: white; line-height: 15px; display: block;">By submitting this form, I agree by electronic signature to: be contacted by SMS text at my mobile 
                    phone number and by email (Consent is not required as a condition of purchase); and 
                    <a class="policy-link" href="https://lifeisbeautiful.com/privacy-policy/">Privacy Policy.</a></span>
                </div>

                <div class="_clear-element"></div>

            </div>
        </div>

        <div class="field">
            <button style="background: <?=$button_bg_color; ?>; color: <?=$button_text_color; ?>; border-color: <?=$button_border_color; ?>;" type="submit" name="saveForm" class="submit-btn" id="_form_5_submit" data-name="saveForm"><?=$button_text;?></button>
        </div>

    </div>
    <div class="_form-thank-you" style="display:none;"></div>

</form>