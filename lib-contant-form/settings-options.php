<?php 

class options_page {

	function __construct() {
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'admin_init', array( $this, 'setup_sections' ) );
        add_action( 'admin_init', array( $this, 'setup_fields' ) );
	}

	function admin_menu() {
		add_options_page(
			'LiB Contact Form',
			'LiB Contact Form Settings',
			'manage_options',
			'options_lib_form_settings',
			array(
				$this,
				'settings_page'
			)
		);
	}

	function  settings_page() { ?>

        <div class="wrap">
            <h2>LiB Form Settings Page</h2>
            <form method="post" action="options.php">
                <?php
                    settings_fields( 'smashing_fields' );
                    do_settings_sections( 'smashing_fields' );
                    submit_button();
                ?>
            </form>
        </div> 
        
        <?php
    }

    public function setup_sections() {
        add_settings_section( 'lib_general_settings', 'General Settings', false, 'smashing_fields' );
        add_settings_section( 'lib_style_settings', 'Button Styles', false, 'smashing_fields' );
    }

    public function setup_fields() {
        $fields = array(
            array(
                'uid' => 'main_heading_text',
                'label' => 'Enter Main Heading',
                'section' => 'lib_general_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                // 'supplemental' => 'Enter main heading text',
                // 'default' => '01/01/2015'
            ),
            array(
                'uid' => 'sub_heading_text',
                'label' => 'Enter Sub Heading',
                'section' => 'lib_general_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                // 'supplemental' => 'Enter sub heading text',
                // 'default' => '01/01/2015'
            ),
            array(
                'uid' => 'button_text',
                'label' => 'Enter Button Text',
                'section' => 'lib_general_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                // 'supplemental' => 'Enter sub heading text',
                // 'default' => '01/01/2015'
            ),
            array(
                'uid' => 'button_bg_color',
                'label' => 'Enter Button BG Color',
                'section' => 'lib_style_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                'supplemental' => 'Enter color hex value for the button background. Ex: #ffffff',
                // 'default' => '01/01/2015'
            ),
            array(
                'uid' => 'button_text_color',
                'label' => 'Enter Button Text Color',
                'section' => 'lib_style_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                'supplemental' => 'Enter color hex value for the button text. Ex: #000000',
                // 'default' => '01/01/2015'
            ),
            array(
                'uid' => 'button_border_color',
                'label' => 'Enter Button Border Color',
                'section' => 'lib_style_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                'supplemental' => 'Enter color hex value for the button border. Ex: #000000',
                // 'default' => '01/01/2015'
            )
        );
        foreach( $fields as $field ){
            add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'smashing_fields', $field['section'], $field );
            register_setting( 'smashing_fields', $field['uid'] );
        }
    }

    public function field_callback( $arguments ) {

        $value = get_option( $arguments['uid'] ); // Get the current value, if there is one
        if( ! $value ) { // If no value exists
            $value = $arguments['default']; // Set to our default
        }
    
        // Check which type of field we want
        switch( $arguments['type'] ){
            case 'text': // If it is a text field
                printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
                break;
        }
    
        // If there is help text
        if( $helper = $arguments['helper'] ){
            printf( '<span class="helper"> %s</span>', $helper ); // Show it
        }
    
        // If there is supplemental text
        if( $supplimental = $arguments['supplemental'] ){
            printf( '<p class="description">%s</p>', $supplimental ); // Show it
        }
    }

}

new options_page;