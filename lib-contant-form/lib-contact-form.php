<?php

/*
Plugin Name: LiB Contact Form
Description: Adds contact form to homepage hero section
Version: 1.0.0
Text Domain: lib-contact-form
Author: R&R Partners
*/


if ( ! defined( 'DO_LIB_DIR' ) ) {
	define( 'DO_LIB_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
}

if ( ! defined( 'DO_LIB_URI' ) ) {
	define( 'DO_LIB_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );
}


/**
 * Enqueue scripts and styles.
 */
function do_lib_scripts() {

    wp_enqueue_script( 'do-lib-scripts', DO_LIB_URI . 'js/scripts.js', array( 'jquery' ), true );

    wp_enqueue_style( 'do-lib-style', DO_LIB_URI . 'css/styles.css' );

}

add_action( 'wp_enqueue_scripts', 'do_lib_scripts' );


// [bartag foo="foo-value"]
function lib_form_func( $atts ) {

    ob_start();

    $main_title = ( get_option('main_heading_text') !== null ) ? '<h3 style="font-size: 28px;">' . get_option('main_heading_text') . '</h3>' : '';
    $sub_title = ( get_option('sub_heading_text') !== null ) ? '<span style="font-size: 16px; color: #fff;">' . get_option('sub_heading_text') . '</span>' : '';
    $button_text = ( get_option('button_text') !== null ) ? get_option('button_text') : '';
    $button_bg_color = ( get_option('button_bg_color') !== null ) ? get_option('button_bg_color') : '';
    $button_text_color = ( get_option('button_text_color') !== null ) ? get_option('button_text_color') : '';
    $button_border_color = ( get_option('button_border_color') !== null ) ? get_option('button_border_color') : '';

    include plugin_dir_path(__FILE__) . 'partials/lib-form-view.php';

    return ob_get_clean();
}

add_shortcode( 'lib-contact-form', 'lib_form_func' );

require_once('settings-options.php');

