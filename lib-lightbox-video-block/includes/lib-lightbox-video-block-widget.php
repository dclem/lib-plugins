<?php


class Lib_Lightbox_Video_Block extends WP_Widget {

	public function __construct() {
		$widget_ops = array(
			'classname' => 'lib_lightbox_video_block',
			'description' => 'A video box using fancybox',
		);
		add_action('wp_ajax_lib_lightbox_video_block_getVideo', array(&$this, 'getVideoAsset'));
		add_action('wp_ajax_lib_lightbox_video_block_getImage', array(&$this, 'getImageAsset'));

		parent::__construct( 'lib_lightbox_video_block', 'LiB Lightbox Video Block', $widget_ops );
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {
		$title = !empty($instance['title']) ? $instance['title'] : '';
		$subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : '';
		$thumbnailID = ! empty( $instance['thumbnailID'] ) ? $instance['thumbnailID'] : '';
		$videoURL = ! empty( $instance['videoURL'] ) ? $instance['videoURL'] : '';
		$videoType = ! empty( $instance['videoType'] ) ? $instance['videoType'] : '';

		$image  = wp_get_attachment_image_src($thumbnailID, 'full');
		wp_enqueue_style( 'lib-fancybox-video-block-styles' );
		?>

		<div class="videoBlock lazyVideoBlock" data-artistid="<?php echo uniqid(); ?>" href="<?php echo $videoURL; ?>" data-src="<?php echo $image[0]; ?>">
			<a href="#"></a>
			<div class="play-btn">
				<i class="far fa-play-circle"></i>
			</div>
			<div class="copy">
				<h2><?php echo $title; ?></h2>
				<p><?php echo $subtitle; ?></p>
			</div>
		</div>
		<?php
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$subtitle = ! empty( $instance['subtitle'] ) ? $instance['subtitle'] : '';
		$thumbnailID = ! empty( $instance['thumbnailID'] ) ? $instance['thumbnailID'] : '';
		$videoURL = ! empty( $instance['videoURL'] ) ? $instance['videoURL'] : '';
		$videoWPID = ! empty( $instance['videoWPID'] ) ? $instance['videoWPID'] : '';
		$videoType = ! empty( $instance['videoType'] ) ? $instance['videoType'] : '';

		$image  = wp_get_attachment_image_src($thumbnailID, 'full');
		$videoExists = '';

		if($videoURL != ''){
			$videoExists = 'show';
		}
		?>
		<div class="lib_lightbox_video_block" id="lib_lightbox_video_block_admin_<?php echo uniqid(); ?>">

			<div class="grid-x grid-padding-x">
				<div class="cell small-6">
					<label>Video Title</label><br/>
					<input type="text" placeholder="" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $title; ?>"><br/>
					<label>Video Subtitle</label><br/>
					<input type="text" placeholder="" name="<?php echo $this->get_field_name('subtitle'); ?>" value="<?php echo $subtitle; ?>">
				</div>
			</div>
			<div class="grid-x grid-padding-x">
				<div class="cell small-6">
					<input type="hidden" id="lib_select_thumb_field" name="<?php echo $this->get_field_name('thumbnailID'); ?>" value="<?php echo $thumbnailID; ?>">
					<a href="#" class="thumbnail" id="lib_select_thumb">
						<?php

						if($thumbnailID == ''){
							?>
							<div class="noImg">
								Add a thumbnail
							</div>
							<?php
						}else{
						?>
							<div class="imgContainer" style="background-image: url(<?php echo $image[0]; ?>);">
							</div>

						<?php } ?>
					</a>
				</div>
			</div>
			<div class="grid-x grid-padding-x">
				<div class="cell small-6">
					<input type="hidden" id="lib_select_video_field" name="<?php echo $this->get_field_name('videoURL'); ?>" value="<?php echo $videoURL; ?>">
					<input type="hidden" id="lib_select_video_field_wpid" name="<?php echo $this->get_field_name('videoWPID'); ?>" value="<?php echo $videoWPID; ?>">
					<input type="hidden" id="lib_select_video_type_field" name="<?php echo $this->get_field_name('videoType'); ?>" value="<?php echo $videoType; ?>">
					<label>Select Video Source
						<select id="videoSource">
							<option value="">------</option>
							<option value="youtube" <?php selected( $videoType, 'youtube' ); ?>>Youtube</option>
							<option value="vimeo" <?php selected( $videoType, 'vimeo' ); ?>>Vimeo</option>
							<option value="upload" <?php selected( $videoType, 'upload' ); ?>>Upload</option>
						</select>
					</label>

					<div class="videoPreview <?php if($videoType == 'youtube') echo 'show';?>" id="youtube">
						<div class="input-group">
							<input class="input-group-field urlProcess" type="text" id="processYoutubeURLInput" placeholder="Ex: https://www.youtube.com/watch?v=R8-WRWs1Z8o">
							<div class="input-group-button">
								<input type="button" class="button" id="processYoutubeURL" value="Embed">
							</div>
						</div>
						<div class="videoBox <?php if(($videoType == 'youtube') && ($videoURL != '')) echo 'active';?>">
							<iframe src="<?php if(($videoType == 'youtube') && ($videoURL != '')) echo $videoURL;?>" frameborder="0" allow="encrypted-media" id="videoBoxEmbed" allowfullscreen></iframe>
						</div>
						<div class="instructions">
							<p>Embeding a youtube video:
								<ol>
									<li>Open the video on youtube</li>
									<li>Copy the video URL</li>
									<li>Paste the URL into the field</li>
									<li>Click "Embed"</li>
								</ol>
							</p>
						</div>

					</div>
					<div class="videoPreview <?php if($videoType == 'vimeo') echo 'show';?>" id="vimeo">
						<div class="input-group">
							<input class="input-group-field urlProcess" type="text" id="processVimeoURLInput" placeholder="Ex: https://vimeo.com/255481019">
							<div class="input-group-button">
								<input type="button" class="button" id="processVimeoURL" value="Embed">
							</div>
						</div>
						<div class="videoBox <?php if(($videoType == 'vimeo') && ($videoURL != '')) echo 'active';?>">
							<iframe src="<?php if($videoType == 'vimeo' && $videoURL != '') echo $videoURL;?>" frameborder="0" allow="encrypted-media" id="videoBoxEmbed" allowfullscreen></iframe>
						</div>
						<div class="instructions">
							<p>Embeding a Vimeo video:
								<ol>
									<li>Open the video on Vimeo</li>
									<li>Click the share button (paper airplane icon) and copy the "Link"</li>
									<li>Paste the URL into the field</li>
									<li>Click "Embed"</li>
								</ol>
							</p>
						</div>
					</div>
					<div class="videoPreview <?php if($videoType == 'upload') echo 'show';?>" id="upload">
						<a href="#" class="button" id="lib_select_video" value="Select Video">Select Video</a>
						<div class="videoBox <?php if(($videoType == 'upload') && ($videoURL != '')) echo 'active';?>">
							<video src="<?php if($videoType == 'upload' && $videoURL != '') echo $videoURL;?>" type="<?php echo get_post_mime_type($videoWPID); ?>" controls></video>
						</div>

					</div>

				</div>
			</div>
		</div>
		<?php


	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['subtitle'] = ( ! empty( $new_instance['subtitle'] ) ) ? strip_tags( $new_instance['subtitle'] ) : '';
		$instance['thumbnailID'] = ( ! empty( $new_instance['thumbnailID'] ) ) ? strip_tags( $new_instance['thumbnailID'] ) : '';
		$instance['videoURL'] = ( ! empty( $new_instance['videoURL'] ) ) ? strip_tags( $new_instance['videoURL'] ) : '';
		$instance['videoWPID'] = ( ! empty( $new_instance['videoWPID'] ) ) ? strip_tags( $new_instance['videoWPID'] ) : '';
		$instance['videoType'] = ( ! empty( $new_instance['videoType'] ) ) ? strip_tags( $new_instance['videoType'] ) : '';

		return $instance;
	}
}