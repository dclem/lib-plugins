<?php
/**
 * LiB Lightbox Video Block
 *
 * @package   LibLightboxVideoBlock
 * @author    Tomas Cordero
 * @copyright Copyright (c) 2017 Tomas, LLC
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: LiB Lightbox Video Block
 * Plugin URI:
 * Description: A video block plugin uisng fancybox
 * Version:     1.0.0
 * Author:      Tomas Cordero
 * Author URI:  https://rrpartners.com
 * License:     GPL-2.0+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: lib-gallery
 */

wp_register_style(
	'lib-admin',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-lightbox-video-block/assets/css/admin.css'
);

wp_register_style(
	'lib-fancybox-video-block-styles',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-lightbox-video-block/assets/css/frontend.css'
);

wp_register_script(
	'lib-admin',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-lightbox-video-block/assets/js/admin.js',
	array( 'media-upload', 'media-views', 'wp-backbone', 'wp-util', 'jquery' )
);

require_once(plugin_dir_path(__FILE__).'/includes/lib-lightbox-video-block-widget.php');

function lib_video_enqueue_media_uploader()
{
    wp_enqueue_script( 'lib-admin' );
	wp_enqueue_style( 'lib-admin' );
}
function register_LibLightboxVideoBox(){
	register_widget('Lib_Lightbox_Video_Block');
}

add_action("admin_enqueue_scripts", "lib_video_enqueue_media_uploader");
add_action('widgets_init', 'register_LibLightboxVideoBox');