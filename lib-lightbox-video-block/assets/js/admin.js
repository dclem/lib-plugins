var media_uploader = null;

function open_media_uploader_image(selector)
{
    media_uploader = wp.media({
        frame:    "post",
        state:    "insert",
        library: {type: 'image'},
        multiple: false
    });

    media_uploader.on("insert", function(){
        var json = media_uploader.state().get("selection").first().toJSON();
        var image_url = json.url;
        var image_caption = json.caption;
        var image_title = json.title;
        if(selector.find('.noImg').length <= 0){
            var noImg = $('<div class="imgContainer"></div>');
                noImg.css({
                    'background-image': 'url(' + image_url + ')'
                })
            selector.find('#lib_select_thumb').html(noImg)
        }else{
            selector.find('.noImg').removeClass('noImg').addClass('imgContainer').css({
                'background-image': 'url(' + image_url + ')'
            }).html('');
        }
		selector.find('#lib_select_thumb_field').val(json.id);
    });
    media_uploader.open();
}
function open_media_uploader_video(selector)
{
    media_uploader = wp.media({
        frame:    "post",
        state:    "insert",
        library: {type: 'video'},
        multiple: false
    });

    media_uploader.on("insert", function(){
        var json = media_uploader.state().get("selection").first().toJSON();
        var video = $('<video />', {
            src: json.url,
            type: json.mime,
            controls: true
        });
        // console.log(selector.find('#upload'))
        selector.find('#upload').addClass('show').find('.videoBox').addClass('active').html(video);
        selector.find('#lib_select_video_field').val(json.url);
        selector.find('#lib_select_video_field_wpid').val(json.id);
    });

    media_uploader.open();
}
function vimeoGetURL(url){
    var vimeoRegex = /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i;
    var parsed = url.match(vimeoRegex);

    console.log(parsed);
    return "//player.vimeo.com/video/" + parsed[1];
};
function youTubeGetURL(url){
  var ID = '';
  url = url.replace(/(>|<)/gi,'').split(/(vi\/|v=|\/v\/|youtu\.be\/|\/embed\/)/);
  if(url[2] !== undefined) {
    ID = url[2].split(/[^0-9a-z_\-]/i);
    ID = ID[0];
  }
  else {
    ID = url;
  }
    return 'https://www.youtube.com/embed/' + ID;
}

$(function(){
	$('body').on('click', '.lib_lightbox_video_block #lib_select_thumb', function(e){
		e.preventDefault();
		open_media_uploader_image($(this).closest('.lib_lightbox_video_block'));
	})
	$('body').on('click', '.lib_lightbox_video_block #processYoutubeURL', function(e){
		e.preventDefault();
        var URL = youTubeGetURL($(this).closest('.lib_lightbox_video_block').find('#processYoutubeURLInput').val());
        $(this).closest('.lib_lightbox_video_block').find('#lib_select_video_field').val(URL);
        $(this).closest('.lib_lightbox_video_block').find('#youtube #videoBoxEmbed').attr('src', URL);
        $(this).closest('.lib_lightbox_video_block').find('#processYoutubeURLInput').val('');
        $(this).closest('.lib_lightbox_video_block').find('#youtube .videoBox').addClass('active');
    });
	$('body').on('click', '.lib_lightbox_video_block #processVimeoURL', function(e){
		e.preventDefault();
        var URL = vimeoGetURL($(this).closest('.lib_lightbox_video_block').find('#processVimeoURLInput').val());
        console.log(URL);
        $(this).closest('.lib_lightbox_video_block').find('#lib_select_video_field').val(URL);
        $(this).closest('.lib_lightbox_video_block').find('#vimeo #videoBoxEmbed').attr('src', URL);
        $(this).closest('.lib_lightbox_video_block').find('#processVimeoURLInput').val('');
        $(this).closest('.lib_lightbox_video_block').find('#vimeo .videoBox').addClass('active');
    });
	$('body').on('change', '.lib_lightbox_video_block #videoSource', function(e){
		e.preventDefault();
        var $thisParent = $(this).closest('.lib_lightbox_video_block');
        $thisParent.find('.videoPreview.show').removeClass('show');
        $thisParent.find('#lib_select_video_type_field').val($(this).val());
        if($(this).val() != 'upload'){
            $thisParent.find('#lib_select_video_field').val($('#' + $(this).val()).find('iframe').attr('src'));
        }else{
            $thisParent.find('#lib_select_video_field').val($('#' + $(this).val()).find('video').attr('src'));
        }
        $thisParent.find('#' + $(this).val()).addClass('show');

	})
	$('body').on('click', '.lib_lightbox_video_block #lib_select_video', function(e){
		e.preventDefault();
		open_media_uploader_video($(this).closest('.lib_lightbox_video_block'));
	})
})