<?php
/**
 * LiB Calendar
 *
 * @package   LibCalendar
 * @author    Tomas Cordero
 * @copyright Copyright (c) 2017 Tomas, LLC
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: LiB Calendar
 * Plugin URI:
 * Description: A Calendar plugin for Life Is Beautiful
 * Version:     1.0.0
 * Author:      Tomas Cordero
 * Author URI:  https://rrpartners.com
 * License:     GPL-2.0+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: lib-calendar
 */

wp_register_style(
	'lib-calendar',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/css/lib-calendar.css'
);
wp_register_style(
	'simple-grid',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/css/simple-grid.min.css'
);
wp_register_style(
	'lib-calendar_frontend',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/css/styles.css'
);
wp_register_style(
	'lib-calendar_jqueryui',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/css/jquery-ui.min.css'
);

wp_register_script(
	'lib-calendar_jqueryui',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/js/jquery-ui.min.js',
	array( 'jquery' )
);
wp_register_script(
	'lib-calendar',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/js/lib-calendar.js',
	array( 'lib-calendar_jqueryui', 'jquery' )
);
wp_register_script(
	'lib-calendar_frontend',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-calendar/assets/js/calendar.js',
	array( 'jquery' )
);


require_once(plugin_dir_path(__FILE__).'/includes/lib-calendar-widget.php');

function lib_calenar_enqueue_media_uploader()
{
    wp_enqueue_script( 'lib-calendar' );

	wp_enqueue_style( 'lib-calendar' );
	// wp_enqueue_style( 'simple-grid' );
	wp_enqueue_style( 'dashicons' );
}
function register_libCalendar(){
	register_widget('Lib_Calendar_Widget');
}
add_action("admin_enqueue_scripts", "lib_calenar_enqueue_media_uploader");
add_action('widgets_init', 'register_libCalendar');