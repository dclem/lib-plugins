<?php
/**
 * LiB Logo Gallery
 *
 * @package   LibLogoSlider
 * @author    Tomas Cordero
 * @copyright Copyright (c) 2017 Tomas, LLC
 * @license   GPL-2.0+
 *
 * @wordpress-plugin
 * Plugin Name: LiB Logo Slider
 * Plugin URI:
 * Description: A Logo Slider plugin for Life Is Beautiful
 * Version:     1.0.0
 * Author:      Tomas Cordero
 * Author URI:  https://rrpartners.com
 * License:     GPL-2.0+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: lib-logo-slider
 */

wp_register_style(
	'lib-logo-slider',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-logo-slider/assets/css/lib-logo-slider.css'
);
wp_register_style(
	'simple-grid',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-logo-slider/assets/css/simple-grid.min.css'
);
wp_register_style(
	'lib-logo-slider_frontend',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-logo-slider/assets/css/styles.css'
);

wp_register_script(
	'lib-logo-slider',
	dirname( plugin_dir_url( __FILE__ ) ) . '/lib-logo-slider/assets/js/lib-logo-slider.js',
	array( 'media-upload', 'media-views', 'wp-backbone', 'wp-util', 'jquery' )
);


require_once(plugin_dir_path(__FILE__).'/includes/lib-logo-slider-widget.php');

function enqueue_logo_media_uploader()
{
    wp_enqueue_script( 'lib-logo-slider' );
	wp_enqueue_style( 'lib-logo-slider' );
	// wp_enqueue_style( 'simple-grid' );
	wp_enqueue_style( 'dashicons' );
}
function register_libLogoSlider(){
	register_widget('Lib_Logo_Slider_Widget');
}
add_action("admin_enqueue_scripts", "enqueue_logo_media_uploader");
add_action('widgets_init', 'register_libLogoSlider');