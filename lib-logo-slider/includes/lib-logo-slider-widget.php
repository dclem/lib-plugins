<?php


class Lib_Logo_Slider_Widget extends WP_Widget {

	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array(
			'classname' => 'lib_logo_slider',
			'description' => 'A Logo Slider Widget',
		);
		add_action('wp_ajax_lib_logo_slider_getImages', array(&$this, 'getLibLogoSliderImages'));

		parent::__construct( 'lib_logo_slider', 'LiB Logo Slider', $widget_ops );
	}

	public function cmp($a, $b){
		return strcmp($a->order, $b->order);
	}

	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	public function widget( $args, $instance ) {

		$direction = !empty($instance['direction']) ? $instance['direction'] : 'ltr';
		$layout = ! empty( $instance['layout'] ) ? $instance['layout'] : 'slider';
		$images = ! empty( $instance['images'] ) ? $instance['images'] : '';
		$imagesObj = json_decode($images);
		// $imgArray = new ArrayObject($imagesObj);
		$orderedImgObjs = (array) $imagesObj;

		wp_enqueue_style( 'lib-logo-slider_frontend' );
		usort($orderedImgObjs, array($this, "cmp"));
		$uniqueID = uniqid();

		if(count($orderedImgObjs) > 0){
			if($layout == 'slider'){
				?><div class="gallery-logo-module"><?php

				foreach($orderedImgObjs as $key => $value){

					$imageFull  = wp_get_attachment_image_src($value->id, 'full');
					$url  = $value->url;
					// print_r($image);
					?>
					<figure data-src="<?php echo $imageFull[0]; ?>" style="background-image: url('<?php echo $imageFull[0]; ?>')">
						<?php if(!empty($url)){?>
							<a href="<?php echo $url; ?>" target="_blank"></a>
						<?php } ?>
					</figure>
					<?php
				}
			}else{
				?><div class="gallery-logo-module_grid"><?php
				foreach($orderedImgObjs as $key => $value){

					$imageFull  = wp_get_attachment_image_src($value->id, 'full');
					$url  = $value->url;
					?>
					<figure data-src="<?php echo $imageFull[0]; ?>" style="background-image: url('<?php echo $imageFull[0]; ?>')">
						<?php if(!empty($url)){?>
							<a href="<?php echo $url; ?>" target="_blank"></a>
						<?php } ?>
					</figure>
					<?php
				}
				?><div class="clear-fix"></div><?php
			}
		}
		?></div><?php
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		$direction = ! empty( $instance['direction'] ) ? $instance['direction'] : 'ltr';
		$layout = ! empty( $instance['layout'] ) ? $instance['layout'] : 'slider';
		$images = ! empty( $instance['images'] ) ? $instance['images'] : '{}';
		$imagesObj = json_decode($images);
		$orderedImgObjs = (array) $imagesObj;
		$uniqueID = uniqid();
		usort($orderedImgObjs, array($this, "cmp"));

		?>
		<div id="lib_gallery_form_<?php echo $uniqueID; ?>" class="lib_logo_slider_form_container grid-container">
			<div class="grid-x">
				<div class="cell small-6">
					<fieldset class="large-6 cell">
						<legend>Select logo farm format</legend>
						<input type="radio" name="<?php echo $this->get_field_name('layout'); ?>"" value="slider" id="formatOptionSlider" <?php checked($layout, 'slider', true); ?>><label for="formatOptionSlider">Slider</label>
						<input type="radio" name="<?php echo $this->get_field_name('layout'); ?>"" value="grid" id="formatOptionGrid" <?php checked($layout, 'grid', true); ?>><label for="formatOptionGrid">Grid</label>
					</fieldset>
				</div>
			</div>
			<div class="grid-x">
				<div class="cell small-9">
					<a class="button-secondary" id="lib_gallery_addImages_logo-item" href="#" ><?php esc_attr_e( 'Add Images' ); ?></a>
				</div>
			</div>

			<div id="lib_gallery_images" class="grid-x grid-margin-x">
				<?php
				if(count($orderedImgObjs) > 0){
					foreach($orderedImgObjs as $key => $value){
						$image  = wp_get_attachment_image_src($value->id, 'full');

						?>
						<div class="cell small-2 imgBlockContainer" id="img_block_<?php echo $value->id; ?>">
							<a class="lib-admin_ui--logo_item--remove" data-parentid="lib_gallery_form_<?php echo $uniqueID; ?>" data-id="<?php echo $value->id; ?>" href="#" title="Remove">X</a>
							<div class="img-preview" style="background-image: url(<?php echo $image[0]; ?>);">
							</div>
							<label for="url">URL
								<input type="text" id="url" name="<?php echo $value->id; ?>" value="<?php echo $value->url; ?>" />
							</label>
							<label for="order">Order
								<input type="number" id="order" min="1" name="<?php echo $value->id; ?>" value="<?php echo $value->order; ?>" />
							</label>
							<input type="hidden" id="" name="" value="<?php echo $value->id; ?>" />

						</div>
						<?php
					}
				}else{
					?>
					<div class="cell noImgs">
						<h3>No images selected</h3>
					</div>
					<?php
				}
				?>
			</div>
			<input type="hidden" id="lib_logo_slider_imgsObj" name="<?php echo $this->get_field_name('images'); ?>" value='<?php echo $images; ?>' />
		</div>

		<?php
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['direction'] = ( ! empty( $new_instance['direction'] ) ) ? strip_tags( $new_instance['direction'] ) : '';
		$instance['layout'] = ( ! empty( $new_instance['layout'] ) ) ? strip_tags( $new_instance['layout'] ) : '';

		$instance['images'] = ( ! empty( $new_instance['images'] ) ) ? strip_tags( $new_instance['images'] ) : '';

		return $instance;
	}

	public function getLibLogoSliderImages(){
		$images = json_decode($_POST['imageids']);
		$parentID = $_POST['parentId'];
		if(count($images) > 0){
			foreach($images as $key => $value){
				$image  = wp_get_attachment_image_src($value, 'full');

				?>
				<div class="cell small-2 imgBlockContainer" id="img_block_<?php echo $value; ?>">
					<a class="lib-admin_ui--logo_item--remove" data-parentid="<?php echo $parentID; ?>" data-id="<?php echo $value; ?>" href="#" title="Remove">X</a>
					<div class="img-preview" style="background-image: url(<?php echo $image[0]; ?>);">
					</div>

					<label for="url">URL
						<input type="text" id="url" name="<?php echo $value->id; ?>" value="<?php echo $value->url; ?>" />
					</label>
					<label for="order">Order
						<input type="number" id="order" min="1" name="<?php echo $value; ?>" value="1" />
					</label>
					<input type="hidden" id="" name="" value="<?php echo $value; ?>" />

				</div>
				<?php
			}
		}

		wp_die();
	}
}