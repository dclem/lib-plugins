<?php
/**
 * @var array $args
 * @var string $title
 * @var bool $arrows
 * @var WP_Query $posts
 * @var string $default_thumbnail
 * @var string $storage_hash
 */

?>

<?php if($posts->have_posts()) : ?>
	<div class="sow-carousel-title sow-carousel-nav-container">

		<?php // if( ! empty( $title ) ) echo $args['before_title'] . esc_html( $title ) . $args['after_title'] ?>

		<?php if (!$arrows) : ?>
			<a href="#" class="sow-carousel-next" title="<?php esc_attr_e('Next', 'so-widgets-bundle') ?>"></a>
			<a href="#" class="sow-carousel-previous" title="<?php esc_attr_e('Previous', 'so-widgets-bundle') ?>"></a>
		<?php endif; ?>
	
	</div>

	<div class="sow-carousel-container<?php if( is_rtl() ) echo ' js-rtl' ?>">

 	<?php if (!$arrows) : ?>
		<a href="#" class="sow-carousel-previous" title="<?php esc_attr_e('Previous', 'so-widgets-bundle') ?>"></a>
		<a href="#" class="sow-carousel-next" title="<?php esc_attr_e('Next', 'so-widgets-bundle') ?>"></a>
	<?php endif; ?>

		<div class="sow-carousel-wrapper"
		     data-found-posts="<?php echo esc_attr($posts->found_posts) ?>"
		     data-ajax-url="<?php echo sow_esc_url( wp_nonce_url( admin_url('admin-ajax.php'), 'widgets_action', '_widgets_nonce' ) ) ?>"
			>
			<ul class="sow-carousel-items <?= (!$disable_carousel) ? '' : 'disable' ?>">
				<?php include plugin_dir_path( __FILE__ ) . 'carousel-post-loop.php' ?>
			</ul>
		</div>
	</div>
	<input type="hidden" name="instance_hash" value="<?php echo esc_attr( $storage_hash ) ?>"/>
<?php endif; ?>
