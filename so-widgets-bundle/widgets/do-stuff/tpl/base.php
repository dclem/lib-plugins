<?php
/**
 * @var array $args
 * @var string $category
 * @var string $type
 */

?>

<?php if ($type == 'cards') : ?>

    <div data-selected-category="<?= $category ?>" data-selected-type="<?= $type ?>" id="ajax-feed" class="featured-artists"></div>

<?php elseif ($type == 'list') : ?>

    <div class="sort-row row">
        <div class="lineup-header-col columns large-6">
            <h4 class="line-up-header"><?= date('Y'); ?> LINE UP</h4>
        </div>
        <div class="columns large-6">
            <select class="lineup-filter-by" name="lineup-filter-by" id="lineup-filter-by-category">
                <option value="all">All</option>
            </select>
        </div>        
    </div>

    <div data-selected-category="<?= $category ?>" id="ajax-feed-list"></div>

<?php elseif ($type == 'featured') : ?>

    <div>Featured</div>

<?php endif; ?>
