<?php
/*
Widget Name: Do Stuff Widget
Description: Display Do Stuff Data
Author: R&R Partners
Author URI: https://rrpartners.com
*/


function do_stuff_get_data() {

	if ( empty( $_REQUEST['_widgets_nonce'] ) || !wp_verify_nonce( $_REQUEST['_widgets_nonce'], 'widgets_action' ) ) return;
	
	$template_vars = array();
	if ( ! empty( $_GET['instance_hash'] ) ) {
		$instance_hash = $_GET['instance_hash'];
		global $wp_widget_factory;
        /** @var SiteOrigin_Widget $widget */
		$widget = ! empty ( $wp_widget_factory->widgets['DoStuff_Widget'] ) ?
            $wp_widget_factory->widgets['DoStuff_Widget'] : null;
		if ( ! empty( $widget ) ) {
            $instance = $widget->get_stored_instance($instance_hash);
            $instance['paged'] = $_GET['paged'];
            $template_vars = $widget->get_template_variables($instance, array());
        }
	}
	ob_start();
	extract( $template_vars );

	include 'tpl/do-stuff-loop.php';

	$result = array( 'html' => ob_get_clean() );
	
	header('content-type: application/json');
	echo json_encode( $result );

	exit();
}

/**
 * @param array $instance The button instance.
 * @param SiteOrigin_Widget $widget The widget object.
 */
function lib_enqueue_admin_scripts(){
	wp_enqueue_script( 'lib-admin-scripts', plugin_dir_url( __FILE__ ) . 'js/dostuff.js', array( 'jquery' ) );
}

add_action( 'wp_ajax_do_stuff_load', 'do_stuff_get_data' );
add_action( 'wp_ajax_nopriv_do_stuff_load', 'do_stuff_get_data' );
add_action( 'admin_enqueue_scripts', 'lib_enqueue_admin_scripts' );

class DoStuff_Widget extends SiteOrigin_Widget {
	function __construct() {

		parent::__construct(
			'lib-do-stuff',
			__('LiB DoStuff', 'lib-do-stuff-widget'),
			array(
				'description' => __('Display DoStuff artists on page.', 'lib-do-stuff-widget'),
				'instance_storage' => true,
				'help' => ''
			),
			array(

			),
			false ,
			plugin_dir_path(__FILE__).'../'
		);
	}

	function initialize() {


		$this->register_frontend_scripts(
			array(
				array(
					'lib-touch-swipe',
					plugin_dir_url( SOW_BUNDLE_BASE_FILE ) . 'js/jquery.touchSwipe.js',
					array( 'jquery' ),
					'1.6.6'
				),
				array(
					'lib-dosuff-scripts',
					plugin_dir_url(__FILE__) . 'js/dostuff.js',
					array( 'jquery' ),
					SOW_BUNDLE_VERSION,
					true
				)
			)
		);
		$this->register_frontend_styles(
			array(
				array(
					'lib-dosuff-styles',
					plugin_dir_url(__FILE__) . 'css/dostuff.css',
					array(),
					SOW_BUNDLE_VERSION
				)
			)
		);
	}

	function get_widget_form(){

		return array(
			'type' => array(
				'type' => 'select',
				'label' => __('Select Type', 'so-widgets-bundle'),
				'default' => 'cards',
				'options' => array(
					'list' => __('List', 'so-widgets-bundle'),
					'cards' => __('Cards', 'so-widgets-bundle'),
					'featured' => __('Featured', 'so-widgets-bundle')
				),
			),	
			'category' => array(
				'type' => 'select',
				'label' => __('DoStuff Category', 'so-widgets-bundle'),
				'default' => 'default',
				'options' => array(
					'all' => __('All Categories', 'so-widgets-bundle'),
					// 'music' => __('Music', 'so-widgets-bundle'),
					// 'art' => __('Art', 'so-widgets-bundle'),
				),
			),
			'artist' => array(
				'type' => 'select',
				'class' => 'artist_ddl',
				'label' => __('Select Artist', 'so-widgets-bundle'),
				'default' => 'default',
				'options' => array(
					'select' => __('Select Artist', 'so-widgets-bundle')
				),
			),

		);
	}

	public function get_template_variables( $instance, $args ) {

		return array(
			'title' => $instance['title'],
			'type' => $instance['type'],
			'category' => $instance['category'],
		);

	}

	function get_template_name($instance){
		return 'base';
	}
}

siteorigin_widget_register('lib-do-stuff', __FILE__, 'DoStuff_Widget');
