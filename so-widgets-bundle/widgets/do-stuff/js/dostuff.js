jQuery(function ($) {

    doStuff = {

        getAllArtists: function () {

            var artists = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_artists/all");
            return artists;

        },

        getSepecificArtist: function (id) {


        },

        getCategories: function () {

            var categoryList = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/lineup_groupings/");

            var categories = [];

            $.each(categoryList.lineup_groupings, function (i, category) {

                categories.push(category.name);

            });

            return categories;

        },

        setCategories: function () {

            var categories = this.getCategories();

            $.each(categories, function(key, value) {
                $('[id$=category]')
                    .append($("<option></option>")
                    .attr("value",value)
                    .text(value));
            });

        },

        setArtistsDDL: function () {

            var artists = this.getAllArtists();

            console.log(artists);

            $.each(artists.festival_artists, function(key, value) {

                $('[id$=artist]')
                    .append($("<option></option>")
                    .attr("value", value.id)
                    .text(value.name));
            });

        },

        artistCardsView: function (selectedCategory, selectedType) {

            var artists = this.getAllArtists();
            var html = '';

            $.each(artists.festival_artists, function (i, artist) {

                if (selectedType === 'cards' && artist.category === selectedCategory || selectedCategory === 'all') {

                    html += '<article class="featured-artist_item artist-block" data-src="' + artist.image + '">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal"></a>';
                    html += '<div class="copy-container">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal"><h2 class="post-card-title">' + artist.name + '</h2></a>';
                    html += '<div class="ds-card-actions">';
                    html += '<a class="ds-card-actions__item" href="' + artist.facebook_url + '">';
                    html += '<i class="fab fa-facebook-f"></i>';
                    html += '</a>';
                    html += '<a class="ds-card-actions__item" href="' + artist.twitter_url + '">';
                    html += '<i class="fab fa-twitter"></i>';
                    html += '</a>';
                    html += '<a class="ds-card-actions__item" href="' + artist.instagram_url + '">';
                    html += '<i class="fab fa-instagram"></i>';
                    html += '</a>';
                    html += '<a class="ds-card-actions__item" href="' + artist.soundcloud_profile_url + '">';
                    html += '<i class="fab fa-soundcloud"></i>';
                    html += '</a>';
                    html += '</div>';
                    html += '</div>';
                    html += '</article>';

                }

            });

            $("#ajax-feed").html(html);

        },

        artistListView: function(selectedCategory) {

            var artists = this.getAllArtists();
            var html = '';

            var categoryStr = selectedCategory.toString();
            var category = categoryStr.toLowerCase();

            html += '<ul class="artist-list">';

            $.each(artists.festival_artists, function (i, artist) {

                if (artist.category === category || category === 'all') {

                    html += '<li><a data-src="#hidden-content" class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal">'+artist.name+'</a></li>';

                }

            });

            html += '</ul>';

            $("#ajax-feed-list").html(html);

        },

        requestAPI: function (urlEndPoint) {

            var returnData = "";

            $.ajax({
                url: urlEndPoint,
                async: false,
                headers: {
                    'DS-API-Key': '93115d892051c9584208ee6b2335442c',
                    'DS-Festival-ID': '30'
                },
                success: function (data) {
                    returnData = data;
                }
            });

            return returnData;

        },

        getModelData: function (artistId) {

            var selectedArtist = this.requestAPI('https://fest-admin.dostff.co/api/v2/festival_artists/' + artistId);
            return selectedArtist;

        }

    }


    var selectedCategory = $("#ajax-feed").attr('data-selected-category');
    var selectedType = $("#ajax-feed").attr('data-selected-type');

    console.log(selectedCategory);

    if (selectedCategory !== undefined) {

        doStuff.artistCardsView(selectedCategory, selectedType);
        doStuff.artistListView(selectedCategory);
        doStuff.setCategories();

    }

    // Do this for the admin section

    $(document).on('panelsopen', function(e) {
        var dialog = $(e.target);

        // Check that this is for our widget class
        if( !dialog.has('.some-unique-widget-form-class') ) return;

        doStuff.setCategories();

        doStuff.setArtistsDDL();

    });

    $('#lineup-filter-by-category').change(function() {

        var selectedFilter = $(this).val();

        doStuff.artistListView(selectedFilter);

    });


});