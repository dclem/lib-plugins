<?php
/**
 * @var $title
 * @var $sub_title
 * @var $title_position
 * @var $url
 * @var $link_attributes
 * @var $new_window
 * @var $attributes
 * @var $classes
 */
?>

<?php if( $title_position == 'above' ) : ?>
	<?php echo $args['before_title'] . wp_kses_post( $title ) . $args['after_title']; ?>
	<p class="imge-sub-title"> <?php echo wp_kses_post( $sub_title ) . $args['after_title']; ?> </p>
<?php endif; ?>

<?php

?>
<div class="grid">

<?php if ( ! empty( $url ) ) : ?><a href="<?php echo sow_esc_url( $url ) ?>" <?php foreach( $link_attributes as $att => $val ) if ( ! empty( $val ) ) : echo $att.'="' . esc_attr( $val ) . '" '; endif; ?>><?php endif; ?>

	<figure class="effect-sadie">

		<img <?php foreach( $attributes as $n => $v ) if ( ! empty( $v ) ) : echo $n.'="' . esc_attr( $v ) . '" '; endif; ?>
			class="<?php echo esc_attr( implode(' ', $classes ) ) ?>"/>
		<?php if ( ! empty( $url ) ) : ?></a><?php endif; ?>

			<?php if( $title_position == 'inner' ) : ?>
				<figcaption>
					<h2><?=wp_kses_post( $title ) ?></h2>
					<p class="imge-sub-title"> <?php echo wp_kses_post( $sub_title ) . $args['after_title']; ?> </p>
				</figcaption>

		<?php endif; ?>
		
	</figure>

</div>

<?php if( $title_position == 'below' ) : ?>
	<?php echo $args['before_title'] . wp_kses_post( $title ) . $args['after_title']; ?>
	<p class="imge-sub-title"> <?php echo wp_kses_post( $sub_title ) . $args['after_title']; ?> </p>
<?php endif; ?>
