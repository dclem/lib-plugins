<?php
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
    header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
?>

<?php if ($type == 'cards') : ?>

    <div class="featured-artists dostuff_artist-cards" data-selected-category="<?= $category ?>" data-card-count="<?= $cardCount ?>"></div>

<?php elseif ($type == 'single') : ?>


<?php elseif ($type == 'list') : ?>

    <div class="artist-lineup-grid">
        <div class="sort-row row">
            <div class="lineup-header-col columns large-6">
                <h4 class="line-up-header"><?= date('Y'); ?> LINEUP</h4>
            </div>
            <div class="columns large-6">
                <!-- <select class="lineup-filter-by" name="lineup-filter-by" id="lineup-filter-by-category">
                    <option value="all">All</option>
                </select> -->
            </div>
        </div>

        <div class="ajax-feed-list dostuff_lineup" data-selected-category="<?= $category ?>"></div>

    </div>

<?php elseif ($type == 'featured') : ?>

    <div class="dostuff_featured-artist" data-selected-category="<?= $category ?>" data-artistid="<?= $artistid ?>">
        <div class="dostuff_featured-artist_info">
            <div class="dostuff_featured-artist_info-category">
                <div class="category">
                </div>
            </div>
            <div class="dostuff_featured-artist_info-name">
                <h2></h2>
                <!-- <a data-artist-id="<? // =$artistid ?>" class="vote-btn btn stage_info login-modal">Add to Schedule</a> -->
            </div>
            <div class="dostuff_featured-artist_info-wrapper">
                <div class="dostuff_featured-artist_info-overlay">
                    <div class="close"><i class="far fa-times-circle"></i></div>
                    <h3>Show Times</h3>
                </div>
                <div class="dostuff_featured-artist_info-bio">
                    <div class="details">
                    </div>
                </div>

                <?php if($hidesettimes != "true") : ?>
                    <div class="dostuff_featured-artist_info-show_info">
                        <a href="#" class="show-times-link">Show Times</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="dostuff_featured-artist_info-embed">
                <div class="music-embed">
                </div>
            </div>
        </div>
        <div class="dostuff_featured-artist_image">
            <div class="bg"></div>
        </div>
        <div class="clear"></div>
    </div>

<?php endif; ?>
