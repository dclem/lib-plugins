<div class="ideas-section">

    <div class="row">

        <div class="columns large-6">

            <div class="row">

                <div id="columns">

                    <div class="dostuff_performers" data-performer-category="<?=$category; ?>"></div>

                </div>

            </div>

        </div>

        <div class="columns large-6">

            <div class="dostuff_featured_performer" data-show-featured-image="<?=$showFeatured; ?>" data-featured-performer="<?=$featuredPerformer; ?>"></div>

        </div>

    </div>

</div>