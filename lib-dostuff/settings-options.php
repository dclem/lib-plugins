<?php 

class dostuff_options_page {

	function __construct() {
        add_action( 'admin_menu', array( $this, 'admin_menu' ) );
        add_action( 'admin_init', array( $this, 'setup_ds_sections' ) );
        add_action( 'admin_init', array( $this, 'setup_ds_fields' ) );
	}

	function admin_menu() {
		add_options_page(
			'LiB DoStuff',
			'LiB DoStuff Settings',
			'manage_options',
			'options_lib_ds_settings',
			array(
				$this,
				'settings_dostuff_page'
			)
		);
	}

	function  settings_dostuff_page() { ?>

        <div class="wrap">
            <h2>LiB DoStuff Settings Page</h2>
            <form method="post" action="options.php">
                <?php
                    settings_fields( 'lib_ds_fields' );
                    do_settings_sections( 'lib_ds_fields' );
                    submit_button();
                ?>
            </form>
        </div> 
        
    <?php }

    public function setup_ds_sections() {
        add_settings_section( 'lib_ds_general_settings', 'General Settings', false, 'lib_ds_fields' );
        // add_settings_section( 'lib_style_settings', 'Button Styles', false, 'lib_ds_fields' );
    }

    public function setup_ds_fields() {
        $fields = array(
            array(
                'uid' => 'ds_button_text',
                'label' => 'Text Field',
                'section' => 'lib_ds_general_settings',
                'type' => 'text',
                'options' => false,
                // 'placeholder' => 'DD/MM/YYYY',
                // 'helper' => 'Does this help?',
                // 'supplemental' => 'Enter sub heading text',
                // 'default' => '01/01/2015'
            )
        );
        foreach( $fields as $field ){
            add_settings_field( $field['uid'], $field['label'], array( $this, 'field_callback' ), 'lib_ds_fields', $field['section'], $field );
            register_setting( 'lib_ds_fields', $field['uid'] );
        }
    }

    public function field_callback( $arguments ) {

        $value = get_option( $arguments['uid'] ); // Get the current value, if there is one
        if( ! $value ) { // If no value exists
            $value = $arguments['default']; // Set to our default
        }
    
        // Check which type of field we want
        switch( $arguments['type'] ){
            case 'text': // If it is a text field
                printf( '<input name="%1$s" id="%1$s" type="%2$s" placeholder="%3$s" value="%4$s" />', $arguments['uid'], $arguments['type'], $arguments['placeholder'], $value );
                break;
        }
    
        // If there is help text
        if( $helper = $arguments['helper'] ){
            printf( '<span class="helper"> %s</span>', $helper ); // Show it
        }
    
        // If there is supplemental text
        if( $supplimental = $arguments['supplemental'] ){
            printf( '<p class="description">%s</p>', $supplimental ); // Show it
        }
    }

}

new dostuff_options_page;