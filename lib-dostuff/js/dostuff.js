jQuery(function ($) {

    $(document).ready(function () {

        // var users = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_users?page=2");
        // console.log(users);

        $.ajaxSetup({
            cache: true
        });


    });


    doStuff = {
        
        fbAccessToken: '',
        doStuffLoginURL: 'https://fest-admin.dostff.co/users/sign_in',

        checkLoginState: function() {
            // console.log('here')
            var _this = this;
            FB.getLoginStatus(function (response) {
                if (response.status === 'connected') {

                    var uid = response.authResponse.userID;
                    var accessToken = response.authResponse.accessToken;
                    
                    var currentUserToken = localStorage.getItem("dsUserToken");
                    
                    // console.log("Past: " + currentUserToken);
                    // console.log("Current: " + accessToken);

                    _this.fbAccessToken = accessToken;

                    var userSignInData = {
                        'token': accessToken
                    }

                    var dsUserLogin = _this.loginUser('https://fest-admin.dostff.co/auth/facebook/callback', userSignInData);
                    
                    // var selectedArtist = doStuff.postRequest("https://fest-admin.dostff.co/api/v2/festival_artists/" + artistId + "/toggle_pick", logUserIn.token);

                    // console.log(dsUserLogin);
                    // Vote
                    
                } else if (response.status === 'not_authorized') {
                    console.log('not logged into system');
                } else {
                    console.log('not logged into anything')
                }
            });
        },
        fbLogOut: function(){
            FB.logout(function(response){
                console.log('logged out');
            });
        },
        getAllArtists: function () {

            var artists = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_artists/all");
            return artists;

        },

        getSepecificArtist: function (id) {
            var artist = this.requestAPI('https://fest-admin.dostff.co/api/v2/festival_artists/' + id);
            return artist;
        },

        getCategories: function () {

            var categoryList = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/lineup_groupings/");

            var categories = [];

            $.each(categoryList.lineup_groupings, function (i, category) {

                categories.push(category.name);

            });

            return categories;

        },

        hasRegistered: function () {
            var userId = localStorage.getItem("dsUserId");
            if (userId !== null) {
                return true;
            } else {
                return false;
            }
        },

        setCategories: function () {

            var categories = this.getCategories();

            $.each(categories, function (key, value) {
                $('[id$=category]')
                    .append($("<option></option>")
                        .attr("value", value)
                        .text(value));
            });

        },

        setArtistsDDL: function () {

            var artists = this.getAllArtists();

            $.each(artists.festival_artists, function (key, value) {

                $('[id$=artist]')
                    .append($("<option></option>")
                        .attr("value", value.id)
                        .text(value.name));
            });

        },

        featuredPerformer: function($el) {

            var elem = $($el);
            
            var performerId = elem.attr('data-featured-performer');
            var showfeaturedimage = elem.attr('data-show-featured-image');

            var featuredPerformer = this.getSepecificArtist(performerId);
            
            var html = '';

            html += '<div class="featured-performer">';
            html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + featuredPerformer.festival_artist.id + '" id="artist-modal">';

            if (showfeaturedimage === 'true') {
            
                html += '<div class="image-cropper featured">';
                html += '<img class="performer-image" src="' + featuredPerformer.festival_artist.image + '">';
                html += '</div></a>';

            }

            html += '<div class="performer-text">';
            html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' +  featuredPerformer.festival_artist.id + '" id="artist-modal"><h2>' +  featuredPerformer.festival_artist.name + '</h2></a>';
            html += '<div>';    
            
            if (featuredPerformer.festival_artist.facebook_url !== null) {
                html += '<a class="ds-card-actions__item" target="_blank" href="' + featuredPerformer.festival_artist.facebook_url + '">';
                html += '<i class="fab fa-facebook-f"></i>';
                html += '</a>';
            }

            if (featuredPerformer.festival_artist.twitter_url !== null) {
                html += '<a class="ds-card-actions__item" target="_blank" href="' + featuredPerformer.festival_artist.twitter_url + '">';
                html += '<i class="fab fa-twitter"></i>';
                html += '</a>';
            }

            if (featuredPerformer.festival_artist.instagram_url !== null) {
                html += '<a class="ds-card-actions__item" target="_blank" href="' + featuredPerformer.festival_artist.instagram_url + '">';
                html += '<i class="fab fa-instagram"></i>';
                html += '</a>';
            }

            if (featuredPerformer.festival_artist.soundcloud_profile_url !== null) {
                html += '<a class="ds-card-actions__item" target="_blank" href="' + featuredPerformer.festival_artist.soundcloud_profile_url + '">';
                html += '<i class="fab fa-soundcloud"></i>';
                html += '</a>';
            }

            html += '</div>';

            elem.html(html);

        },

        performerView: function($el) {

            var elem = $($el);

            var count = elem.attr('data-performer-count');
            var category = elem.attr('data-performer-category');
            var performers = this.getAllArtists();

            var html = '';

            $.each(performers.festival_artists, function (i, performer) {


                if (performer.category === category) {

                    html += '<div class="ideas-performer__container">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + performer.id + '" id="artist-modal">';
                    html += '<div class="image-cropper">';
                    html += '<img class="performer-image" src="' + performer.image + '">';
                    html += '</div></a>';
                    html += '<div class="performer-text">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + performer.id + '" id="artist-modal"><h3 class="post-card-title">' + performer.name + '</h3></a>';
                    html += '<div>';
                    if (performer.facebook_url !== null) {
                        html += '<a class="ds-card-actions__item" target="_blank" href="' + performer.facebook_url + '">';
                        html += '<i class="fab fa-facebook-f"></i>';
                        html += '</a>';
                    }

                    if (performer.twitter_url !== null) {
                        html += '<a class="ds-card-actions__item" target="_blank" href="' + performer.twitter_url + '">';
                        html += '<i class="fab fa-twitter"></i>';
                        html += '</a>';
                    }

                    if (performer.instagram_url !== null) {
                        html += '<a class="ds-card-actions__item" target="_blank" href="' + performer.instagram_url + '">';
                        html += '<i class="fab fa-instagram"></i>';
                        html += '</a>';
                    }

                    if (performer.soundcloud_profile_url !== null) {
                        html += '<a class="ds-card-actions__item" target="_blank" href="' + performer.soundcloud_profile_url + '">';
                        html += '<i class="fab fa-soundcloud"></i>';
                        html += '</a>';
                    }
                    html += '</div>';
                    html += '</div>';
                    html += '</div>';

                }
            
                return i < 5;

            });

            elem.html(html);

        },

        artistCardsView: function ($el) {

            var elem = $($el);

            var artists = this.getAllArtists();
            var count = elem.attr('data-card-count');
            var category = elem.attr('data-selected-category');

            var html = '';

            $.each(artists.festival_artists, function (i, artist) {

                if (artist.category === category || category === 'all') {

                    html += '<article style="width: 100%" class="featured-artist_item artist-block" data-src="' + artist.image + '">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal"></a>';
                    html += '<div class="copy-container">';
                    html += '<a data-src="#hidden-content" class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal"><h2 class="post-card-title">' + artist.name + '</h2></a>';
                    html += '<div class="ds-card-actions">';

                    if (artist.facebook_url !== null) {
                        html += '<a class="ds-card-actions__item" href="' + artist.facebook_url + '">';
                        html += '<i class="fab fa-facebook-f"></i>';
                        html += '</a>';
                    }

                    if (artist.twitter_url !== null) {
                        html += '<a class="ds-card-actions__item" href="' + artist.twitter_url + '">';
                        html += '<i class="fab fa-twitter"></i>';
                        html += '</a>';
                    }

                    if (artist.instagram_url !== null) {
                        html += '<a class="ds-card-actions__item" href="' + artist.instagram_url + '">';
                        html += '<i class="fab fa-instagram"></i>';
                        html += '</a>';
                    }

                    if (artist.soundcloud_profile_url !== null) {
                        html += '<a class="ds-card-actions__item" href="' + artist.soundcloud_profile_url + '">';
                        html += '<i class="fab fa-soundcloud"></i>';
                        html += '</a>';
                    }

                    html += '</div>';
                    html += '</div>';
                    html += '</article>';

                }

            });

            elem.html(html);

        },

        artistListView: function ($el) {

            var elem = $($el);

            var selectedCategory = elem.attr('data-selected-category');

            var artists = this.getAllArtists();
            var html = '';

            var categoryStr = selectedCategory.toString();
            var category = categoryStr.toLowerCase();

            html += '<ul class="artist-list">';

            $.each(artists.festival_artists, function (i, artist) {

                if (artist.category === category || category === 'all') {

                    html += '<li><a class="artist-modal" data-artist-id="' + artist.id + '" id="artist-modal">' + artist.name + '</a></li>';

                }

            });

            html += '</ul>';

            elem.html(html);

        },

        requestAPI: function (urlEndPoint) {

            var returnData = "";

            $.ajax({
                url: urlEndPoint,
                async: false,
                headers: {
                    'DS-API-Key': '93115d892051c9584208ee6b2335442c',
                    'DS-Festival-ID': '30',
                    'Content-Type': 'application/json'
                },
                success: function (data) {
                    returnData = data;
                }
            });

            return returnData;

        },

        registerUser: function(urlEndPoint, dataIn) {

            var returnData = "";

            $.ajax({
                type: "POST",
                url: urlEndPoint,
                data: dataIn,
                async: false,
                headers: {
                    'DS-API-Key': '93115d892051c9584208ee6b2335442c',
                    'DS-Festival-ID': '30'
                },
                success: function (data) {
                    returnData = data;
                }
            });

            return returnData;

        },

        loginUser: function(urlEndPoint, dataIn) {

            var returnData = "";
            $.ajax({
                type: "POST",
                url: urlEndPoint,
                data: dataIn,
                async: false,
                headers: {
                    'DS-API-Key': '93115d892051c9584208ee6b2335442c',
                    'DS-Festival-ID': '30'
                },
                success: function (data) {
                    returnData = data;
                    localStorage.setItem("dsUserToken", returnData.token);
                }
            });

            return returnData;

        },

        postRequest: function (urlEndPoint, token) {

            var returnData = "";

            $.ajax({
                type: "POST",
                url: urlEndPoint,
                // data: dataIn,
                async: false,
                headers: {
                    'DS-API-Key': '93115d892051c9584208ee6b2335442c',
                    'DS-Festival-ID': '30',
                    'Authorization': 'Bearer ' + token
                },
                success: function (data) {
                    returnData = data;
                }
            });

            return returnData;

        },

        getModelData: function (artistId) {

            var selectedArtist = this.requestAPI('https://fest-admin.dostff.co/api/v2/festival_artists/' + artistId);
            return selectedArtist;

        },
        truncateString: function (string, n, useWordBoundary) {
            if (string.length <= n) {
                return string;
            }
            var subString = string.substr(0, n - 1);
            return (useWordBoundary ? subString.substr(0, subString.lastIndexOf(' ')) : subString) + "&hellip;";
        },
        getFeaturedArtist: function ($el) {
            var _this = this;
            var artistID = $el.data('artistid'),
                artistData = _this.getSepecificArtist(artistID),
                category = artistData.festival_artist.category,
                artistName = artistData.festival_artist.name,
                artistBio = artistData.festival_artist.bio,
                artistImage = artistData.festival_artist.image,
                artistShowTimes = artistData.festival_artist.performances,
                artistEmbed = artistData.festival_artist.music_embed;

            $el.find('.dostuff_featured-artist_info-category .category').text(category);
            $el.find('.dostuff_featured-artist_info-name h2').text(artistName);
            if(artistBio != null){
                $el.find('.dostuff_featured-artist_info-bio .details').html(_this.truncateString(artistBio, 300, true)).find('p').append(' <a class="artist-modal" data-artist-id="' + artistID + '" data-src="#hidden-content">Read More</a>');
            }
            if(artistEmbed != null){
                $el.find('.dostuff_featured-artist_info-embed .music-embed').html(artistEmbed);
            }
            $el.find('.dostuff_featured-artist_image .bg').css('background-image', 'url(' + artistImage + ')');

            $.each(artistShowTimes, function (key, value) {
                var date = new Date(value.start);
                var $lineItem = $("<div></div>").addClass('time'),
                    $showTime = $('<div></div>').addClass('left'),
                    $stage = $('<div></div>').addClass('right'),
                    $clear = $('<div></div>').addClass('clear');

                $showTime.text(_this.formatDate(date));
                $stage.text(value.stage);
                $lineItem.append($showTime);
                $lineItem.append($stage);
                $lineItem.append($clear);
                $el.find('.dostuff_featured-artist_info-overlay').append($lineItem);
            })
        },
        formatDate: function (date) {
            var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'PM' : 'AM';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + '' + ampm;
            return days[date.getDay()] + ', ' + months[date.getMonth()] + " " + date.getDate() + ' - ' + strTime;
        }

    }
    $.getScript('https://connect.facebook.net/en_US/sdk.js', function () {
        FB.init({
            appId: '499235196895327',
            version: 'v2.7'
        });
        doStuff.checkLoginState();
        // console.log(doStuff.fbAccessToken);
        // $('#loginbutton,#feedbutton').removeAttr('disabled');


    });
    $('.dostuff_featured-artist').each(function () {
        $this = $(this);
        doStuff.getFeaturedArtist($this);
    })

    $('.dostuff_featured_performer').each(function () {
        $this = $(this);
        doStuff.featuredPerformer($this);
    })

    $('.dostuff_artist-cards').each(function () {
        $this = $(this);
        doStuff.artistCardsView($this);
    });

    $('.dostuff_performers').each(function () {
        $this = $(this);
        doStuff.performerView($this);
    });

    $(".dostuff_lineup").each(function () {
        $this = $(this);
        doStuff.artistListView($this);
        doStuff.setCategories();
    });

    // doStuff.setArtistsDDL();

    $('.show-times-link').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.dostuff_featured-artist_info-wrapper');

        $parent.find('.dostuff_featured-artist_info-overlay').addClass('active');
    });
    $('.dostuff_featured-artist_info-overlay .close').click(function (e) {
        e.preventDefault();
        var $parent = $(this).closest('.dostuff_featured-artist_info-wrapper');

        $parent.find('.dostuff_featured-artist_info-overlay').removeClass('active');
    });
    $('#lineup-filter-by-category').change(function () {

        var selectedFilter = $(this).val();

        var list = $(this).parent().parent().next('div');

        $(list).attr('data-selected-category', selectedFilter);

        doStuff.artistListView(list);

    });

    $('#register-btn').on('click', function (e) {

        var artistId = localStorage.getItem("artistId");
        var email = $('#emailAddress').val();
        var fullname = $('#fullName').val();
        var password = $('#password').val();
        var email_opt_in = $('#optin').is( ':checked' ) ? 1: 0;

        if (email == "" || fullname == "" || password == "") {
            $(".form-alert").css('display', 'block');
            return false;
        }

        var userData = {
            'user': {
                'email': email,
                'name': fullname,
                'password': password,
                'mail_list_opt_in': email_opt_in
            }
        }

        var userInfo = doStuff.registerUser("https://fest-admin.dostff.co/users", userData);

        var artistId = localStorage.getItem("artistId");
        localStorage.setItem("dsUserId", userInfo.user.id);

        var userSignInData = {
            'user': {
                'email': email,
                'password': password,
            }
        }

        var logUserIn = doStuff.loginUser(doStuff.doStuffLoginURL, userSignInData);
        var selectedArtist = doStuff.postRequest("https://fest-admin.dostff.co/api/v2/festival_artists/" + artistId + "/toggle_pick", logUserIn.token);
        var userSelection = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_users/" + userInfo.user.id);

        localStorage.setItem("dsUserToken", logUserIn.token);

        $.fancybox.close();
        e.preventDefault();

    });

    $(document).on('click', '#login-btn', function(e){

        var artistId = localStorage.getItem("artistId");
        var email = $('#emailAddress').val();
        var password = $('#password').val();

        if (email == "" || password == "") {
            $(".form-alert").css('display', 'block');
            return false;
        }

        var userSignInData = {
            'user': {
                'email': email,
                'password': password,
            }
        }

        var logUserIn = doStuff.loginUser(doStuff.doStuffLoginURL, userSignInData);
        var selectedArtist = doStuff.postRequest("https://fest-admin.dostff.co/api/v2/festival_artists/" + artistId + "/toggle_pick", logUserIn.token);
        var userSelection = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_users/" + logUserIn.user.id);

        localStorage.setItem("dsUserId", logUserIn.user.id);
        localStorage.setItem("dsUserToken", logUserIn.token);

        $.fancybox.close();
        e.preventDefault();

    });

    $(document).on('click', '.login-link', function(e){

        $(this).css('display', 'none');
        $('.register-link').css('display', 'block');

        $('.recieve-updates').css('display', 'none');

        $('.register-title').html('Login');
        $('#fullName').css('display', 'none');
        $('#login-btn').css('display', 'block');
        $('#register-btn').css('display', 'none');

        e.preventDefault();
    });

    $(document).on('click', '.register-link', function(e){

        $(this).css('display', 'none');
        $('.login-link').css('display', 'block');

        $('.register-title').html('Register');

        $('.recieve-updates').css('display', 'block');
        $("#fullName").css('display', 'block');
        $('#login-btn').css('display', 'none');
        $('#register-btn').css('display', 'block');

        e.preventDefault();
    });

    $(document).on('click', '.vote-btn', function (e) {

        var artistId = $(this).attr('data-artist-id');
        localStorage.setItem('artistId', artistId);

        var userId = localStorage.getItem("dsUserId");
        var dsUser = doStuff.hasRegistered();

        if (dsUser) {

            var userToken = localStorage.getItem("dsUserToken");
            var selectedArtist = doStuff.postRequest("https://fest-admin.dostff.co/api/v2/festival_artists/"+artistId+"/toggle_pick", userToken);

            if (selectedArtist.picked) {
                $(this).text('Remove From Schedule');
            } else {
                $(this).text('Add to Schedule');
            }

        } else {

            $.fancybox.open({
                src: '#login-modal',
                type: 'inline',
                opts: {
                    buttons: [
                        'close'
                    ],
                    margin: 0,
                }
            });

        }

        e.preventDefault();

    });

    $(document).on('click', '.artist-modal', function () {

        $this = $(this);
        var wasSelected = false;

        var userId = localStorage.getItem("dsUserId");
        var selectedArtist = $this.data('artist-id');
        if(userId != null){
            var userSelection = doStuff.requestAPI("https://fest-admin.dostff.co/api/v2/festival_users/" + userId);
            $.each(userSelection.artist_picks, function (key, value) {
                if(value.id === selectedArtist) {
                    wasSelected = true;
                }
            })
        }



        $.fancybox.open({
            src: '#hidden-content',
            type: 'inline',
            opts: {
                buttons: [
                    'close'
                ],
                margin: 0,
                beforeShow: function (elm, current) {

                    var data = doStuff.getModelData(selectedArtist);

                    artistHtml = '<div id="artist-image" class="artist-image" style="background-image: url(' + data.festival_artist.image + ')"></div>';
                    artistHtml += '<div class="artist-content-wrapper">';

                    artistHtml += '<div class="artist-title-wrapper">';

                    artistHtml += '<div class="artist-social">';

                        // if (wasSelected) {
                        //     artistHtml += '<a href="" class="vote-btn btn stage_info" data-artist-id="'+data.festival_artist.id+'">Remove from Schedule</a>';
                        // } else {
                        //     artistHtml += '<a href="" class="vote-btn btn stage_info" data-artist-id="'+data.festival_artist.id+'">Add to Schedule</a>';
                        // }

                        if (data.festival_artist.facebook_url !== null) {
                            artistHtml += '<a target="_blank" href="' + data.festival_artist.facebook_url + '"><i class="fab fa-facebook-f"></i></a>';
                        }
                        if (data.festival_artist.instagram_url !== null) {
                            artistHtml += '<a target="_blank" href="' + data.festival_artist.instagram_url + '"><i class="fab fa-instagram"></i></a>';
                        }
                        if (data.festival_artist.twitter_url !== null) {
                            artistHtml += '<a target="_blank" href="' + data.festival_artist.twitter_url + '"><i class="fab fa-twitter"></i></a>';
                        }
                        if (data.festival_artist.youtube_channel_url !== null) {
                            artistHtml += '<a target="_blank" href="' + data.festival_artist.youtube_channel_url + '"><i class="fab fa-youtube"></i></a>';
                        }

                    artistHtml += '</div>'

                    artistHtml += '<div id="artist-name" class="artist-name"><h4>' + data.festival_artist.name + '</h4></div>';

                    if (data.festival_artist.web_site_url !== null) {
                        artistHtml += '<div id="artist-name" class="artist-website"><a target="_blank" href="' + data.festival_artist.web_site_url + '">Visit our website</a></div>';
                    }

                    artistHtml += '</div>';

                    if (data.festival_artist.music_embed !== null) {
                        artistHtml += '<div class="artist-music-block">' + data.festival_artist.music_embed + '</div>';
                    }

                    if (data.festival_artist.video_embed !== null) {
                        artistHtml += '<div class="artist-video-block">' + data.festival_artist.video_embed + '</div>';
                    }

                    if (data.festival_artist.bio !== null) {
                        artistHtml += '<div class="artist-bio-block">' + data.festival_artist.bio + '</div>';
                    }

                    artistHtml += '</div>';

                    $("#artist-content").html(artistHtml);

                },
                afterClose: function (element) {
                    $("#artist-content").html('');
                }
            }
        })


    });

});