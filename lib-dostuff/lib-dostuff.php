<?php

/*
Plugin Name: LiB DoStuff
Description: Adds DoStuff Modules
Version: 1.0.0
Text Domain: lib-dostuff
Author: R&R Partners
*/


if ( ! defined( 'DS_LIB_DIR' ) ) {
	define( 'DS_LIB_DIR', trailingslashit( plugin_dir_path( __FILE__ ) ) );
}

if ( ! defined( 'DS_LIB_URI' ) ) {
	define( 'DS_LIB_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );
}


/**
 * Enqueue scripts and styles.
 */
function dostuff_scripts() {

    wp_enqueue_script( 'ds-lib-scripts', DS_LIB_URI . 'js/dostuff.js', array( 'jquery' ), true );

    wp_enqueue_style( 'ds-lib-style', DS_LIB_URI . 'css/dostuff.css' );

}

add_action( 'wp_enqueue_scripts', 'dostuff_scripts' );

function lib_dostuff_func( $atts = []) {

    ob_start();

    $type = $atts['type'];
    $category = $atts['category'];
    $artistid = $atts['artistid'];
    $hidesettimes = $atts['hidesettimes'];
    $cardCount = ($atts['count']) ? $atts['count'] : 10;

    include plugin_dir_path(__FILE__) . 'partials/lib-dostuff-view.php';

    return ob_get_clean();

}

function lib_ideas_func( $atts = []) {

    ob_start();

    $category = $atts['category'];
    $featuredPerformer = $atts['featured'];
    $showFeatured = $atts['showfeaturedimage'];
    $performerCount = ($atts['count']) ? $atts['count'] : 6;

    include plugin_dir_path(__FILE__) . 'partials/lib-dostuff-ideas-view.php';

    return ob_get_clean();

}

add_shortcode( 'lib-dostuff', 'lib_dostuff_func' );
add_shortcode( 'lib-ideas-dostuff', 'lib_ideas_func' );

require_once('settings-options.php');

