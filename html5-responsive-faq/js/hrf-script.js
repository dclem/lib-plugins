jQuery(function ($) {
    $(document).ready(function () {

        $('.hrf-entry').click(function () {
            if(!$(this).hasClass('faq-open')){

                $('.hrf-entry.faq-open').find('.hrf-title').removeClass('open-faq');
                $('.hrf-entry.faq-open').removeClass('faq-open').find('.hrf-content').slideUp();
                $(this).addClass('faq-open').find('.hrf-title').addClass('close-faq').addClass('open-faq');
                $(this).find('.hrf-content').slideDown();
            }else{
                $(this).find('.hrf-title').removeClass('open-faq');
                $(this).removeClass('faq-open').find('.hrf-content').slideUp();
            }
        }); //.hrf-title click
    });

});