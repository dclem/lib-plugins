<?php
function fn_hrf_styles()
{

   $main_title_size   = ( get_option('hrf_main_title_size') != '' ) ? get_option('hrf_main_title_size', '18px') : '18px';
   $question_color    = ( get_option('hrf_question_text_color') != '' ) ? get_option('hrf_question_text_color', '#444444') : '#444444';
   $question_bgcolor  = ( get_option('hrf_question_bgcolor') != '' ) ? get_option('hrf_question_bgcolor', '#ffffff') : '#ffffff';
   $question_size     = ( get_option('hrf_question_text_size') != '' ) ? get_option('hrf_question_text_size', '18px') : '18px';
   $answer_color      = ( get_option('hrf_answer_text_color') != '' ) ? get_option('hrf_answer_text_color', '#444444') : '#444444';
   $answer_bgcolor    = ( get_option('hrf_answer_bgcolor') != '' ) ? get_option('hrf_answer_bgcolor', '#ffffff') : '#ffffff';
   $answer_size       = ( get_option('hrf_answer_text_size') != '' ) ? get_option('hrf_answer_text_size', '14px') : '14px';
   $bullets_style     = ( get_option('bullets_style') != '' ) ? get_option('bullets_style', 'light') : 'light';
   $bullets_bgcolor   = ( get_option('hrf_bullets_bgcolor') != '' ) ? get_option('hrf_bullets_bgcolor', '#444444') : '#444444';
   $faqs_bottom_gap   = ( get_option('hrf_faqs_bottom_gap') != '' ) ? get_option('hrf_faqs_bottom_gap', '0px') : '0px';
   $heading_style     = ( get_option('hrf_question_headingtype') != '' ) ? get_option('hrf_question_headingtype', 'h3') : 'h3';
   $css = '<style type="text/css">';

   $css .= '
            h2.frq-main-title{
               font-size: '.$main_title_size.';
            }
            .hrf-entry{
               border-bottom: 1px solid black;
               margin-bottom: 15px !important;
               padding-bottom: 10px !important;
            }
            .hrf-content{
               display:none;
               color: '.$answer_color.';
               background: '.$answer_bgcolor.';
               font-size: '.$answer_size.';
               /* padding: 10px ;
               padding-left: 25px; */
               word-break: normal;
            }
            .hrf-content p{
               color: '.$answer_color.';
               font-size: '.$answer_size.';
               word-break: normal;
               margin: 0;
            }
            div.hrf-title{
               font-size: '.$question_size.' ;
               color: '.$question_color.';
               background: '.$question_bgcolor.';
               /* padding: 10px ;
               padding-left: 25px; */
                   font-family: AN-Reg;
                font-weight: 800;
                line-height: 1em;
               margin: 0;
               -webkit-touch-callout: none;
               -webkit-user-select: none;
               -khtml-user-select: none;
               -moz-user-select: none;
               -ms-user-select: none;
               user-select: none;
               outline-style:none;
               position: relative;
            }
            .hrf-title.close-faq{
               cursor: pointer;
            }
            .hrf-title span{
                width: auto;
                height: auto;
                display: block;
                position: absolute;
                left: -17px;
                top: 0;
                margin-right: 0;
                margin-left: 0px;
                font-size: 22px;
                transition: transform .25s ease;
            }
            .hrf-title.close-faq span{

            }
            }.hrf-title.open-faq{

            }
            .hrf-title.open-faq span{
                transform: rotate(90deg);
            }
            .hrf-entry p{

            }
            .hrf-entry ul{

            }
            .hrf-entry ul li{

            }';
   $css .= '</style>';
   echo $css;
}
add_action( 'wp_footer','fn_hrf_styles' );