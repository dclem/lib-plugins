<?php

add_shortcode('hrf_faqs', 'fn_hrf_faqs');

function fn_hrf_faqs($attr)
{
    $faq_params = shortcode_atts( array(
            'category' => '',
            'title' => '',
        ), $attr );

    $html = '<div class="hrf-faq-list">';

    if( $faq_params['title'] != ''){
    $html .= '<h2 class="frq-main-title">'.$faq_params['title'].'</h2>';

    }
    $head_tag  = get_option('hrf_question_headingtype','h3');
    $faq_args = array(
            'post_type'      => 'hrf_faq',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby'        => 'menu_order',
            'order'          => 'ASC',
    );

    if( $faq_params['category'] != '' ){
        $faq_args['category_name'] = $faq_params['category'];
    }

    $faq_query = new WP_Query( $faq_args );

    if( $faq_query->have_posts() ):
        $counter = 1;
        while( $faq_query->have_posts() ):
            $faq_query->the_post();
            if($counter == 1){
                $firstClass = ['faq-open','open-faq','display: block;'];
            }else{
                $firstClass = ['','',''];
            }
            $html .= '<article class="hrf-entry '.$firstClass[0].'" id="hrf-entry-'.$faq_query->post->ID.'">
                        <div class="hrf-title close-faq '.$firstClass[1].'" data-content-id="hrf-content-'.$faq_query->post->ID.'"><span><i class="fas fa-caret-right"></i></span>'.get_the_title().'</div>
                        <div class="hrf-content" id="hrf-content-'.$faq_query->post->ID.'" style="'.$firstClass[2].'">'.apply_filters( 'the_content', get_the_content() ).'</div>
                    </article>';
            $counter++;
        endwhile;
    else:
        $html .= "No FAQs Found";
    endif;
    wp_reset_query();
    $html .= '</div>';
    return $html;
}